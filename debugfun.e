
@global DFlogmessage
/************************************************************************/
/*      logmessage - appends/starts a log file of messages.             */
/*                                                                      */
/*      On the host, there is no easy way to printf things, so we       */
/*      follow this process for debugging:                              */
/*      1) Open a file for write (or re-write file) then close          */
/*      2) For each message, open-write-close file                      */
/*                                                                      */
/*      Also, if this is running on the simulator, printf messages      */
/*      *DO* appear, so we can just printf them!                        */
/*                                                                      */
/*      INPUT:                                                          */
/*              logfilename = path and name of file                     */
/*              logstring = character string to write to file           */
/*              rewrite = 1 to rewrite, 0 to append          		*/
/*		sim_control = 1 to file, 0 to printf			*/
/*									*/
/*									*/
/* Inline this code into the global section				*/
/************************************************************************/
#ifdef PSD_HW           /* hardware */
	static char logfile[] = "/usr/g/bin/DPFGlog";
	/*int simlog_control = 0; */
#else			/* Simulator */    
	static char logfile[] = "DPFGlog";
	/*int simlog_control = 1;*/ /* 0=printf, 1=print to the file logfilesim*/
#endif

int write_control = 1;

int logmessage( char *logfilename, char *logstring, int rewrite, int sim_control);


int logmessage( char *logfilename, char *logstring, int rewrite, int sim_control)
{
FILE *logfile;
int openok=0;

#ifdef PSD_HW           /* Only do this on scanner (not simulator) */
	if (rewrite==1)
    		logfile = fopen(logfilename,"w");
	else
    		logfile = fopen(logfilename,"a");

	if (logfile != NULL)
        {
        	openok = 1;
        	fprintf(logfile,"%s\n",logstring);
        	fclose (logfile);
        }

#else	/* Simulator */
	if(sim_control == 1){
		if (rewrite==1)
    			logfile = fopen(logfilename,"w");
		else
    			logfile = fopen(logfilename,"a");

		if (logfile != NULL)
        	{	
        		openok = 1;
        		fprintf(logfile,"%s\n",logstring);
        		fclose (logfile);
        	}
	}else{
        	fprintf(stderr,"%s\n",logstring);
        	openok = 1;     /* Don't return error message if simulator! */
	}
#endif

return (openok);
}



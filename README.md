# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Source code for double-diffusion multi-band EPI sequence 
* Version 1.1

### How do I get set up? ###
	1. Download repository
	2. in terminal run: prep_psd_dir 
	3. (set ese_xx DV25/26...) 
	4. make sure you have the repository bz_common in the same base folder as generic_dpfg. 
	5. psdqmake clean sim or psdqmake clean hw

* Configuration

### Dependencies and setup on scanner ###
	1. Create folder /usr/g/research/bzahneisen (on scanner)
	2. copy DiffusionVectors_XXX_00.dvs (XXX=DTI,DSE,HSE,SSE) to /usr/g/research/bzahneisen
	3. copy rf-pulse files (bz_sinc.rho and bz_sinc.theta) to /usr/g/research/bzahneisen


### How to run tests and simulate sequence ###
	1. start WTools
	2. Analysis->Evaltool
	3. click on Load cv & 
	4. load file named cvs
	5. set EvalMode to manual
	6. click EVAL multiple times (wait for ca. 2s between clicks) untile you see "predownload done" in WToolsMgd message window
	7.click SAVE


* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* benjamin.zahneisen@stanford.edu
* Other community or team contact
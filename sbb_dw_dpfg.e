/*
 *  sbb_dw_dpfg.e
 *
 *  This file contains all the external functions
 *  defined in sbb_dw_dpfg.h
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sept/2016
 *
 */

/**********************************************/
@global _global
#include "sbb_dw_dpfg.h"
#define MAXCHAR 150




/**********************************************/
@host _ipgexport


/**********************************************/
@cv _cv
/* CVs: RF1 and gradients, etc */




/*****************************************/
@host _host
#include "sbb_dw_dpfg.h"

void sbb_dw_dpfg_init(sbb_dw_dpfg* p){
    
    p->prfpulse = NULL;
    
    /*initialize structures*/
    sbb_diff_grad_init(&(p->m_dgrad));
    sbb_diff_grad_init(&(p->m_dgrad2));
    sbb_refoc_init(&(p->m_sbb180));
    
    p->Ninstances			= 0; /*how often do we employ that pulse per entry point */
    p->mode = DSE;
    p->alpha = 90;
    p->antiparallel = 0;
    
    /*timing*/
    p->start_time           = 0;
    p->total_time           = 25000;
    p->time_to_RF1          = 10000; /*sbb duration measured from peak of rf pulse */
    p->time_from_RF2        = 10000;
    p->time_RF1_to_RF2      = 0;
    p->tmix                 = 12000;
    p->tmixMin              = 12000;
    p->tfill1               = 10000;
    p->tfill2               = 0;
    p->tdelta_lobes         = 0;
    p->moment1              = 10000;
    p->moment2              = 20000;
    p->useMaxLogGrad	    = 1;
    p->debug                = 0;

};

#define GAUSS_cm_2_TESLA_m (1.0E-2)
#define TWOPI_GAMMA_SI (TWO_PI*42.58E6)
#define USEC_2_SEC (1.0E-6)
#define SEC_2_USEC (1.0E6)
#define MIN_DELTA 8
#define TWO_PI 2.0*PI 
#define GAMMA 42.58E6

STATUS sbb_dw_dpfg_cveval(sbb_dw_dpfg* psbb,mplx_pulse* prf,float bval1,float bval2)
{
	/*pssb : pointer to sbb-structure 
	  float bval1 : b-value of first diffusion pair in [s/mm^2] as in opval
	  float bval12 : b-value of 2nd diffusion pair in [s/mm^2] as in opval*/

	/*calculates internal timing for minTE */
	
	double t180;
	double a,b;
	double x[3];
	int nsol;
	double G;
	double bval1_SI,bval2_SI;
	int moment1,moment2;
	float gradpower_1axis;
	char message[256];
	
    if(psbb->debug)
      fprintf(stderr, "\n welcome to sbb_dw_dpfg_cveval()\n");


    /*assign pointer to rf-pulse*/
    psbb->prfpulse = &(prf[0]);

    psbb->m_sbb180.prfpulse = psbb->prfpulse; /*pass on pointer to rf-pulse*/
    
    /*prepare 180deg pulse (used twice/3x)*/
    sbb_refoc_cveval(&(psbb->m_sbb180));
	
    /*get total time of 180 in seconds*/
    if(psbb->mode == SSE)
      t180 = 0;
    else
      t180 = (double) psbb->m_sbb180.total_time * USEC_2_SEC; /*sec*/

	/*get max gradient amplitude G [T/m] */
    	/*gettarget(&target, XGRAD, &loggrd);*/
	if(psbb->useMaxLogGrad){	
	  gradpower_1axis = FMin(3,loggrd.tx,loggrd.ty,loggrd.tz);
	}else{
	  gradpower_1axis = FMin(3,loggrd.tx_xyz,loggrd.ty_xyz,loggrd.tz_xyz);
      }
      
	G = (double) gradpower_1axis *GAUSS_cm_2_TESLA_m;

	a = G*G*TWOPI_GAMMA_SI*TWOPI_GAMMA_SI*(2.0/3.0);
	b = G*G*TWOPI_GAMMA_SI*TWOPI_GAMMA_SI*t180;
	
	/*calculate and prepare 1st gradient pair*/
	bval1_SI = (double) bval1 *(1000.0*1000.0); /*[s/m2]*/
	
	SolveCubic(a,b,0.0,-bval1_SI,&nsol,x);
	
	if(nsol==1){
	  moment1 = (int) (x[0]*SEC_2_USEC) *gradpower_1axis;
	  fprintf(stderr, "sbb_dw_dpfg_cveval: moment1 = %d. \n",moment1);
	}
	else{
	  moment1 = (int) (FMax(3,x[0],x[1],x[2])*SEC_2_USEC) *gradpower_1axis;
	}

	/*prepare diffusion gradient pulse G1*/
	sbb_diff_grad_cveval(&(psbb->m_dgrad),moment1);
	
	sprintf(message,"sbb_dw_dpfg_cveval: moment1 = %d. \n",moment1);
	logmessage( logfile,message, 0, 1); /*1= to file*/ 
    
	/*calculate and prepare 2nd gradient pair (if not DTI/DSI)*/
	if(!(psbb->mode==DTI)){ 
	    bval2_SI = (double) bval2 *(1000.0*1000.0); /*[s/m2]*/
	    SolveCubic(a,b,0.0,-bval2_SI,&nsol,x);
	  
	    if(nsol==1){
	      moment2 = (int) (x[0]*SEC_2_USEC) *gradpower_1axis;
	    }
	    else{
	      moment2 = (int) (FMax(3,x[0],x[1],x[2])*SEC_2_USEC) *gradpower_1axis;
	    }
	
	    /*prepare diffusion gradient pulse G2*/
	    sbb_diff_grad_cveval(&(psbb->m_dgrad2),moment2);
	}
	
	/*calculates the timing for minTE (no restrictions from excitation pulse or readout)*/
	switch(psbb->mode){
		
		case SSE:
			psbb->tfill1 = 0;
			psbb->tfill2 = 0;
			psbb->tmixMin = psbb->m_dgrad.total_time;
			psbb->time_to_RF1 = 2*psbb->m_dgrad.total_time + psbb->tdelta_lobes + RUP_GRD(psbb->m_sbb180.total_time/2);
			psbb->time_from_RF2 = 2*psbb->m_dgrad2.total_time + psbb->tdelta_lobes + RUP_GRD(psbb->m_sbb180.total_time/2);
			break;
		
		case DSE:
			psbb->tmix = psbb->m_dgrad.total_time;
			psbb->tmixMin = psbb->m_dgrad.total_time;	
			psbb->tfill1 = 0;
			psbb->tfill2 = 0;
			psbb->time_to_RF1 = psbb->m_dgrad.total_time + RUP_GRD(psbb->m_sbb180.total_time/2);
			psbb->time_from_RF2 = psbb->m_dgrad2.total_time + RUP_GRD(psbb->m_sbb180.total_time/2);
			psbb->time_RF1_to_RF2 = psbb->m_sbb180.total_time + psbb->m_dgrad.total_time + psbb->m_dgrad2.total_time +  psbb->tfill1;	    
			psbb->total_time = 2*psbb->m_dgrad.total_time + 2*psbb->m_sbb180.total_time + 2*psbb->m_dgrad2.total_time +  psbb->tfill1;
			break;

		case HSE:

			if(psbb->m_dgrad.total_time < psbb->m_dgrad2.total_time){
			    psbb->tfill1 = psbb->m_dgrad2.total_time - psbb->m_dgrad.total_time; /* > 0*/
			    psbb->tfill2 = 0;
			    psbb->tmix = psbb->m_dgrad.total_time + psbb->m_sbb180.total_time + psbb->tfill1;
			    psbb->tmixMin= psbb->m_dgrad.total_time + psbb->m_sbb180.total_time + psbb->tfill1;
			}
			else{
			    psbb->tfill1 = 0;
			    psbb->tfill2 = psbb->m_dgrad.total_time - psbb->m_dgrad2.total_time; /* > 0*/
			    psbb->tmix = psbb->m_dgrad.total_time + psbb->m_sbb180.total_time + psbb->tfill2;
			    psbb->tmixMin= psbb->m_dgrad.total_time + psbb->m_sbb180.total_time + psbb->tfill2;    
			}
			psbb->time_to_RF1 = psbb->m_dgrad.total_time + RUP_GRD(psbb->m_sbb180.total_time/2);
			psbb->time_from_RF2 = psbb->m_dgrad2.total_time + RUP_GRD(psbb->m_sbb180.total_time/2);
			break;

		case DTI:
			psbb->tfill1 = 0;
			psbb->tfill2 = 0;
			psbb->tmix   = 0;
			psbb->tmixMin= 0;
			psbb->m_dgrad2.total_time =0;
			psbb->time_to_RF1 = psbb->m_dgrad.total_time + RUP_GRD(psbb->m_sbb180.total_time/2) ;
			psbb->time_from_RF2 = psbb->time_to_RF1;
			psbb->time_RF1_to_RF2 = 0;
			psbb->total_time = 2*psbb->m_dgrad.total_time + psbb->m_sbb180.total_time;
			break;
	}
   
    /*reset number of instances with every call of cveval*/
    psbb->Ninstances = 0;
    
    if(psbb->debug)
        fprintf(stderr, "done sbb_dw_dpfg_cveval()\n");

    return SUCCESS;
}



STATUS sbb_dw_dpfg_updateTE(sbb_dw_dpfg* psbb,int TE, int dTE ){

	switch(psbb->mode){
	    
	    case SSE:
		psbb->tfill1 = RUP_GRD(dTE/2);
		psbb->tfill2 = RUP_GRD(dTE/2);
		psbb->time_to_RF1 = 2*psbb->m_dgrad.total_time + psbb->tfill1 + psbb->tdelta_lobes + psbb->m_sbb180.total_time/2;
		psbb->time_from_RF2 = 2*psbb->m_dgrad2.total_time + psbb->tfill2 + psbb->tdelta_lobes + psbb->m_sbb180.total_time/2;
		psbb->time_RF1_to_RF2 =0;
		psbb->total_time = 2*psbb->m_dgrad.total_time + 2*psbb->m_dgrad2.total_time + 2*psbb->tdelta_lobes + psbb->tfill1 + psbb->tfill2 + psbb->m_sbb180.total_time;
	      break;
	    case DSE:
		psbb->tfill1 = TE/2 - (psbb->m_sbb180.total_time + psbb->m_dgrad.total_time + psbb->m_dgrad2.total_time );
		psbb->tfill2 = 0;
		psbb->tmix = psbb->m_dgrad.total_time + psbb->tfill1;
		psbb->time_to_RF1 = psbb->m_dgrad.total_time + RUP_GRD(psbb->m_sbb180.total_time/2);/*does not change with dTE >0*/
		psbb->time_from_RF2 = psbb->m_dgrad2.total_time + RUP_GRD(psbb->m_sbb180.total_time/2); /*does not change with dTE >0*/
		psbb->time_RF1_to_RF2 = psbb->m_sbb180.total_time + psbb->m_dgrad.total_time + psbb->m_dgrad2.total_time +  psbb->tfill1;
		psbb->total_time = 2*psbb->m_dgrad.total_time + 2*psbb->m_sbb180.total_time + 2*psbb->m_dgrad2.total_time +  psbb->tfill1;
	      break;
	  case HSE:
	      psbb->tfill1 = RUP_GRD(TE/4 - (psbb->m_sbb180.total_time + psbb->m_dgrad.total_time  ));
	      psbb->tfill2 = RUP_GRD(TE/4 - (psbb->m_sbb180.total_time + psbb->m_dgrad2.total_time  ));
	      psbb->tmix = psbb->m_dgrad.total_time + psbb->tfill1 +psbb->tfill2 + psbb->m_sbb180.total_time;
	      psbb->total_time = 2*psbb->m_dgrad.total_time + 3*psbb->m_sbb180.total_time + 2*psbb->m_dgrad2.total_time +  psbb->tfill1 + psbb->tfill2;
	      psbb->time_RF1_to_RF2 = 2*psbb->m_sbb180.total_time + psbb->m_dgrad.total_time + psbb->m_dgrad2.total_time +  psbb->tfill1 + psbb->tfill2;
	    break;
	  case DTI:
	     fprintf(stderr, "nothing to be done here. \n");
	    break;
      }
    	

      if(psbb->debug)
        fprintf(stderr, "done sbb_dw_dpfg_updateTE()\n");

	return SUCCESS;
};



STATUS sbb_dw_dpfg_predownload(void){



    return SUCCESS;
};


/* Subroutine for minte_for_bval */
void SolveCubic(double  a,         /* coefficient of x^3 */
		double  b,         /* coefficient of x^2 */
		double  c,         /* coefficient of x   */
		double  d,         /* constant term      */
		int    *nsol, /* # of distinct solutions */
		double *x)         /* array of solutions      */
{
  double    a1 = b/a, a2 = c/a, a3 = d/a;
  double    Q = (a1*a1 - 3.0*a2)/9.0;
  double    R = (2.0*a1*a1*a1 - 9.0*a1*a2 + 27.0*a3)/54.0;
  double    R2_Q3 = R*R - Q*Q*Q;
  double    theta;

  if (R2_Q3 <= 0){
    *nsol = 3;		
    theta = acos(R/sqrt(Q*Q*Q));
    x[0] = -2.0 * sqrt(Q) * cos( theta/3.0) - a1/3.0;		
    x[1] = -2.0 * sqrt(Q) * cos( (theta+2.0*PI)/3.0) - a1/3.0;
    x[2] = -2.0 * sqrt(Q) * cos( (theta+4.0*PI)/3.0) - a1/3.0;
  } else {		
    *nsol = 1;		
    x[0] = pow( sqrt(R2_Q3) + fabs(R), 1/3.0);
    x[0] += Q/x[0];
    x[0] *= (R < 0.0) ? 1 : -1;
    x[0] -= a1/3.0;
  }
  
}


/* end @host rfov_funcs.e ***************************/


/*************************************************/
@pg _pg



STATUS sbb_dw_dpfg_pulsegen(sbb_dw_dpfg* psbb,CHAR* sbb_name,mplx_pulse* prf,int Nwaves)
{
    /*sbb_dw_dpfg* psbb : pointer to sbb_dw_dpfg structure
     sbb_name           : name of sequence building block
     prf                : pointer to rf pulse structure from sequence
     */

    if(psbb->debug){
        fprintf(stderr, "\nwelcome to sbb_dw_dpfg_pulsegen()\n");
		psbb->m_dgrad.debug = psbb->debug;
		psbb->m_dgrad2.debug =psbb->debug;		
	}

    /*local variables */
    int t;
    char name[128]; 
    
    /*re-assign pointer to rf-pulse*/
    psbb->prfpulse = &(prf[0]);
    /*psbb->m_sbb180.prfpulse = psbb->prfpulse;*/
    
    /*start timing*/
    t = psbb->start_time;

    /*G1_1 */  
    sprintf(name,"%s_%s",sbb_name,"m_dgrad_G11");
    sbb_diff_grad_pulsegen(&(psbb->m_dgrad),name,t);
    t += psbb->m_dgrad.total_time;
    
    /*1st 180deg pulse*/
    if(!(psbb->mode == SSE)){
      /*psbb->m_sbb180.start_time = t;*/
      sprintf(name,"%s_%s",sbb_name,"m_180_1");
      sbb_refoc_pulsegen(&(psbb->m_sbb180),name,&(prf[0]),t);
      t += psbb->m_sbb180.total_time;
    }
    
    /*G1_2 */
    sprintf(name,"%s_%s",sbb_name,"m_dgrad_G12");
    sbb_diff_grad_pulsegen(&(psbb->m_dgrad),name,t);
    t += psbb->m_dgrad.total_time;
    
    /*add fill time*/
    t += psbb->tfill1;
	
    /*3rd 180-pulse in the middle in case of HSE (or SSE) mode*/
    if((psbb->mode==HSE) || (psbb->mode == SSE)){
		/*psbb->m_sbb180.start_time = t;*/
		sprintf(name,"%s_%s",sbb_name,"m_180c");
		sbb_refoc_pulsegen(&(psbb->m_sbb180),name,&(prf[0]),t);
		t += psbb->m_sbb180.total_time;
	}
    
    /*add 2nd tfill (if different from zero)*/
    t += psbb->tfill2;
    
    if(!(psbb->mode==DTI)){
    
      /*G21 */
      sprintf(name,"%s_%s",sbb_name,"m_dgrad_G21");
      sbb_diff_grad_pulsegen(&(psbb->m_dgrad2),name,t);
      t += psbb->m_dgrad2.total_time;
    
      if( (psbb->mode==DSE) || (psbb->mode==HSE)){
      /*2nd 180deg pulse*/
	/*psbb->m_sbb180.start_time = t;*/
	sprintf(name,"%s_%s",sbb_name,"m_180_2");
	sbb_refoc_pulsegen(&(psbb->m_sbb180),name,&(prf[0]),t);
	t += psbb->m_sbb180.total_time;
      }
    
      /*G22 */
      sprintf(name,"%s_%s",sbb_name,"m_dgrad_G22");
      sbb_diff_grad_pulsegen(&(psbb->m_dgrad2),name,t);
      t += psbb->m_dgrad2.total_time;
    }  

    /*prepare multiple waveforms*/
    sbb_refoc_pulsegen_pomp(&(psbb->m_sbb180),Nwaves,prf);

    /*set total duration*/
    psbb->total_time = t - psbb->start_time;

    if(psbb->debug)
    fprintf(stderr, "sbb_dw_dpfg_pulsegen() finished. \n" );

    return SUCCESS;
};



/*************************************************/
/* EXPORT: write the timing to a file*/
/*************************************************/
int sbb_dw_dpfg_writeLog(sbb_dw_dpfg* psbb,char* fname2){

    FILE *file;

    char fname[128];
    sprintf(fname,"%s",fname2);
    file = fopen(fname,"w");

    if (file == NULL) {
       fprintf(stderr,"ERROR: unable to open ASCII waveform file\n");
       fflush(stderr);
    }

    if (file != NULL) {

        /*print timing information */
        fprintf(file, "total_time [microsec] = %d: \n",psbb->total_time);
        fprintf(file, "time_to_RF1 = %d: \n",psbb->time_to_RF1);
	fprintf(file, "time_from_RF2 = %d: \n",psbb->time_from_RF2);
        fprintf(file, "time_RF1_to_RF2 = %d: \n",psbb->time_RF1_to_RF2);
	fprintf(file, "tmix = %d: \n",psbb->tmix);
	fprintf(file, "tfill1 = %d: \n",psbb->tfill1);
	fprintf(file, "tfill2 = %d: \n",psbb->tfill2);

    }

    if (file != NULL) {
       fclose(file);
    }

    return 1;
};





/***********************************/
@rsp _rsp
void sbb_dw_dpfg_psdinit(sbb_dw_dpfg* psbb){

	/*initialize sbbs*/
	sbb_diff_grad_psdinit(&(psbb->m_dgrad));
	sbb_refoc_psdinit(&(psbb->m_sbb180));
	if(!(psbb->mode==DTI))
	sbb_diff_grad_psdinit(&(psbb->m_dgrad2));
	

	if(psbb->mode == HSE){
	   int ia; 
	  ia = psbb->m_sbb180.ia_rfExt;/*get instruction amplitude from 180*/
	  ia = (int) ia * (psbb->alpha/180.0);
	  setiamp(ia, &(psbb->m_sbb180.rf), 0); /*where do we get the correct ampliutde ?*/
	  setiamp(-ia, &(psbb->m_sbb180.rf), 2); /*where do we get the correct ampliutde ?*/
	
	}

	if(psbb->debug){
	  fprintf(stderr, "sbb_dw_dpfg_psdinit(): done.\n");
	}

};

void sbb_dw_dpfg_setfrequency(sbb_dw_dpfg* psbb,long freq){

	sbb_refoc_setfrequency(&(psbb->m_sbb180),freq);

	if(psbb->debug){
	    fprintf(stderr, "sbb_dw_dpfg_setfrequency(): done.\n");
	}

};


void sbb_dw_dpfg_setphase(sbb_dw_dpfg* psbb,double phase){
    /*phase value in radians  (e.g. pi/2)*/
	sbb_refoc_setphase(&(psbb->m_sbb180),phase);
    /*setphase(phase, &psbb->rf, INSTRALL);*/
};

/* Sets different waveforms  */
void sbb_dw_dpfg_setwave(sbb_dw_dpfg* psbb,int num)
{
    if(num < 0){
        /*set wave to default single wave pointer*/
        return;
    }

	sbb_refoc_setwave(&(psbb->m_sbb180),num);
	
	if(psbb->mode == HSE){
	   int ia; 
	    ia = psbb->m_sbb180.ia_rfExt * (psbb->m_sbb180.B1scaling[num]);
	  ia = (int) ia * (psbb->alpha/180);
	  setiamp(ia, &(psbb->m_sbb180.rf), 0); /*where do we get the correct ampliutde ?*/
	  setiamp(-ia, &(psbb->m_sbb180.rf), 2); /*where do we get the correct ampliutde ?*/
		fprintf(stderr, "sbb_dw_dpfg_setwave()ia=%i: done.\n",ia);
	}
	
	if(psbb->debug){
		fprintf(stderr, "sbb_dw_dpfg_setwave(): done.\n");
	}
}


void sbb_dw_dpfg_diffstep(sbb_dw_dpfg* psbb,float tensor[][MAX_DTI_DIRECTIONS + MAX_T2],int d){
	
	/*psbb 		= pointer to sbb structure*/
	/*tensor 	= ponter to diffusion tensor (TENSOR_AGP)*/
	/*d 		= which entry of the tensor array */	
	
	/*d must have six elements*/
	if((psbb->debug) && (d>-1) ){
		fprintf(stderr, "TENSOR_AGP_1[:]=%f,%f,%f.\n",tensor[0][d],tensor[1][d],tensor[2][d]);
		fprintf(stderr, "TENSOR_AGP_2[:]=%f,%f,%f.\n",tensor[3][d],tensor[4][d],tensor[5][d]);		
	}

	/*depending on the mode we set the gradient amplitude here*/
	if(d == NO_DIF_GRAD)
	{
		sbb_diff_grad_step(&(psbb->m_dgrad),0.0,0.0,0.0,-1); /*-1 = INSTRALL*/
		if(!(psbb->mode==DTI))
		sbb_diff_grad_step(&(psbb->m_dgrad2),0.0,0.0,0.0,-1); /*-1 = INSTRALL*/		
	}
	else
	{
		switch(psbb->mode){
		
			case SSE:
				if(psbb->antiparallel){
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  	 /* +G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),-tensor[0][d],-tensor[1][d],-tensor[2][d],1);  /* -G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],0);    /* -G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],1); /* +G2_2 */
				}
				else{
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  	 /* G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),-tensor[0][d],-tensor[1][d],-tensor[2][d],1);  /* -G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],0);    /* G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],1); /* -G2_2 */
				}
				break;
			
			case DSE:
				if(psbb->antiparallel){		
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  /* +G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],1);  /* +G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],0); /* -G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],1); /* -G2_2 */
				}
				else{
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  /* G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],1);  /* G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],0); /* G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],1); /* G2_2 */
				}
				break;
			
			case HSE:
				if(psbb->antiparallel){		
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  /* +G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],1);  /* +G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],0); /* -G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),-tensor[3][d],-tensor[4][d],-tensor[5][d],1); /* -G2_2 */
				}
				else{
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  /* G1_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],1);  /* G1_2 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],0); /* G2_1 */
				  sbb_diff_grad_step(&(psbb->m_dgrad2),tensor[3][d],tensor[4][d],tensor[5][d],1); /* G2_2 */
				}
				break;
			case DTI:
				sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],0);  /* G1_1 */
				sbb_diff_grad_step(&(psbb->m_dgrad),tensor[0][d],tensor[1][d],tensor[2][d],1);  /* G1_2 */
				break;			
			default:
				fprintf(stderr, "sbb_dw_dpfg_diffstep(): mode unknown.\n");		
		}
	
	}

};


/**********************************************************/
/* Modifiy crusher gradients to prevent unwanted echoes*/
/**********************************************************/
void sbb_dw_dpfg_crusherTheme(sbb_dw_dpfg* psbb,int crushertheme){
	

    fprintf(stderr,"Crusher theme in sbb_dw_dpfg %i.\n",crushertheme);
    
    /*make sure we have more than once instance*/
    if(psbb->m_sbb180.Ninstances == 1){
      fprintf(stderr,"Only one 180deg pulse present n=%i.\n",psbb->m_sbb180.Ninstances);
      return;
    }	

    int ia;
    
    switch (crushertheme) {
	
	case 0 : 	    /*for dual echo*/
	    /* flip sign of all crusher gradients*/
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gxcrushl),1);
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gxcrushr),1);
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gycrushl),1);
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gycrushr),1);
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gzcrushl),1);
	    setiampt(-psbb->m_sbb180.ia_gcrush,&(psbb->m_sbb180.gzcrushr),1);
	break;

	case 1 :
	    /* flip sign of all crusher gradients AND lower amplitude*/
	    ia = (int) (0.75*(psbb->m_sbb180.ia_gcrush));
	    setiampt(-ia,&(psbb->m_sbb180.gxcrushl),1);
	    setiampt(-ia,&(psbb->m_sbb180.gxcrushr),1);
	    setiampt(-ia,&(psbb->m_sbb180.gycrushl),1);
	    setiampt(-ia,&(psbb->m_sbb180.gycrushr),1);
	    setiampt(-ia,&(psbb->m_sbb180.gzcrushl),1);
	    setiampt(-ia,&(psbb->m_sbb180.gzcrushr),1);
	break;

	default:
	    sbb_refoc_crusherTheme(&(psbb->m_sbb180),1);

    }
      
	
};



/**********************************************************/
/* BZ read gradient table from file*/
/**********************************************************/
int readAndSetDiffGradTable_AGP(int fileIDNr,CHAR* fname_base,int mode){
	
      /*there must be a global array TENSOR_AGP */
      /*mode = 0 --> DTI, read in 3 vector components
      /*mode = 1 --> SSE/DSE/HSE, read in 2x3 vector components*/

      
      char fname[256];
      char lineContent[256];
      char dummy[128];
      int Ndirections;
      int n,j;
      float xtmp1,xtmp2,ytmp1,ytmp2,ztmp1,ztmp2;
      int counter;
	
      #ifdef SIM
	  sprintf(fname,"%s_%02d.dvs",fname_base,fileIDNr);
	  FILE * fp=fopen(fname,"r");
      #else
	  sprintf(fname,"%s/%s_%02d.dvs","/usr/g/research/bzahneisen",fname_base,fileIDNr); 	
	  FILE * fp=fopen(fname,"r");
      #endif

        if(debugTensor == PSD_ON) {
	      fprintf(stderr, "Reading from file: %s \n",fname);
	}


    /*Assign T2 images first */
    for( j = 0; j < num_B0; ++j ) {
        TENSOR_AGP[0][j] = TENSOR_AGP[1][j] = TENSOR_AGP[2][j] = TENSOR_AGP[3][j] = TENSOR_AGP[4][j] = TENSOR_AGP[5][j] =  0.0;
    }

    /*read header information*/
    fgets (lineContent,256, fp);
    sscanf(lineContent, "# Set of %d directions", &Ndirections);
    fgets (lineContent,256, fp);
    sscanf(lineContent, "%s", dummy);
    fgets (lineContent,256, fp);
    sscanf(lineContent, "[directions=%d]", &Ndirections);
    fgets (lineContent,256, fp);
    sscanf(lineContent, "CoordinateSystem = %s", dummy);
    /*fprintf(stderr,"%s \n",lineContent);*/
    fgets (lineContent,256, fp);
    sscanf(lineContent, "Normalization = %s", dummy);

    /* Assign Diffusion Weighted images */
    max_g_norm =  0.0;
    n=num_B0;
    while((fgets(lineContent,256, fp) != NULL) && (n < (MAX_DTI_DIRECTIONS + MAX_T2)) ){

        /* get the tensor values from the file */
	if(mode == 1){
	  sscanf(lineContent, "Vector[%d] = ( %f, %f, %f, %f, %f, %f )",&counter, &xtmp1,&ytmp1,&ztmp1,&xtmp2,&ytmp2,&ztmp2);
	}
	else{
	  sscanf(lineContent, "Vector[%d] = ( %f, %f, %f )",&counter, &xtmp1,&ytmp1,&ztmp1);
	}
    
        if(debugTensor == PSD_ON) {
		fprintf(stderr, "tmpx = %f, tmpy=%f, tmpz= %f\n", xtmp1, ytmp1, ztmp1 );
	}

        /*assign values*/
        TENSOR_AGP[0][n] = xtmp1; TENSOR_AGP[1][n] = ytmp1; TENSOR_AGP[2][n] = ztmp1;
	if(mode == 1){
	  TENSOR_AGP[3][n] = xtmp2; TENSOR_AGP[4][n] = ytmp2; TENSOR_AGP[5][n] = ztmp2;
	}
	else{
	  TENSOR_AGP[3][n] = 0; TENSOR_AGP[4][n] = 0; TENSOR_AGP[5][n] = 0;
	}

        /*  Update max gradient */
        if ((TENSOR_AGP[0][j]*TENSOR_AGP[0][j] +
             TENSOR_AGP[1][j]*TENSOR_AGP[1][j] +
         TENSOR_AGP[2][j]*TENSOR_AGP[2][j]   ) > max_g_norm){

          max_g_norm = TENSOR_AGP[0][j]*TENSOR_AGP[0][j] + TENSOR_AGP[1][j]*TENSOR_AGP[1][j] + TENSOR_AGP[2][j]*TENSOR_AGP[2][j];
        }

        n++;
    }

    if(debugTensor == PSD_ON) {
        fprintf(stderr,"reached end of file. %d lines read \n", n);
        fprintf(stderr,"setting num_tenso = %d lines read \n", n);
    }
    
    /*close file*/ 
    fclose(fp);

    /*return number of directions*/
    Ndirections = n-1;
   
    return Ndirections;
}

 /* This function gets the tensor values from Jones Tensor and puts them into TENSOR_AGP */
STATUS set_tensor_orientationsAGP( void )
{
    /*int read_from_file = PSD_ON;*/
    int j;
    

        FILE *fp;                           		/* file pointer */
        char *tensor_datafile="tensor.dat"; 		/* filename of tensor.dat file */
        /*char *jonestensor_datafile="jonestensor.dat"; 	/* filename of tensor.dat file */
	
#ifndef SIM
        char *tensor_datapath="/usr/g/bin/";		/* path to tensor.dat files - hw  */
#else /* !SIM */
        char *tensor_datapath="./";         	/* path to tensor.dat files - sim */
#endif /* SIM */

        char filestring[MAXCHAR];           		/* buffer used to open data file */ 
        char compstring[MAXCHAR];           		/* buffer used to open data file */ 
        char tempstring[MAXCHAR];           		/* buffer to access file */
        int max_chars_per_line = MAXCHAR;   		/* lines in tensor.dat header */ 
       
        int num_tensor_len;
 
	
        /* Setup for file search - set the number of directions requested */
        sprintf( compstring, "%d", num_tensor );
        num_tensor_len = strlen( compstring );

	/* Stefan Skare. First check if jonestensor.dat */
        /* Set jonestensor.dat file path and append filename base and suffix */
        strcpy( filestring, tensor_datapath );
        strcat( filestring, tensor_datafile );

	  
	  /* Open file */
	  if( (fp = fopen( filestring, "r" )) == NULL ) { 
            printf("Cant read /usr/g/bin/tensor.dat\n" );
	    
            return FAILURE;
	  }
	
	fprintf(stderr,"DTI: Found %s\n",filestring);fflush(stdout);



        /*
         * The tensor.dat file is a concatanation of several files.
         * We need to skip over all the lines until we reach the location
         * that stores the "num_tensor" orientations.
         */
        {
            int read_skip = 1;

            while( read_skip ) {
                fgets( tempstring, max_chars_per_line, fp );
                read_skip = strncmp( compstring, tempstring, num_tensor_len );
            }
        }

        if(debugTensor == PSD_ON) {
            printf( "Tensor Directions Read (AGP) = %d\n", num_tensor );
        }


        /*Assign T2 images first */
        for( j = 0; j < num_B0; ++j ) {
            
	    TENSOR_AGP[0][j] = TENSOR_AGP[1][j] = TENSOR_AGP[2][j] = 0.0;
	    
            if(debugTensor == PSD_ON) {
                printf( "T2 #%d, X = %f, Y=%f, Z= %f\n", j, TENSOR_AGP[0][j], TENSOR_AGP[1][j], TENSOR_AGP[2][j] );
                fflush( stdout );
            }

        }

        /* Assign Diffusion Weighted images */
	max_g_norm =  0.0; 

        for ( j = num_B0; j < num_tensor + num_B0; ++j ) {          
            
	    /* get the tensor file */
	    if( fgets( tempstring, MAXCHAR, fp ) == NULL ) { 
                printf( "ERROR: invalid tensor.dat file format!\n" ); 
            }  
	    
	    /* get the tensor values from the file */        
            sscanf( tempstring, "%f %f %f", &TENSOR_AGP[0][j], &TENSOR_AGP[1][j], &TENSOR_AGP[2][j] );
            
	    /* assign the values */
            /*TENSOR_AGP[0][j] = TENSOR_AGP_temp[0][j];
            TENSOR_AGP[1][j] = TENSOR_AGP_temp[1][j];
            TENSOR_AGP[2][j] = TENSOR_AGP_temp[2][j];*/

	    /*  Update max gradient */
	    if ((TENSOR_AGP[0][j]*TENSOR_AGP[0][j] + TENSOR_AGP[1][j]*TENSOR_AGP[1][j] + TENSOR_AGP[2][j]*TENSOR_AGP[2][j]) > max_g_norm) 
	      max_g_norm = TENSOR_AGP[0][j]*TENSOR_AGP[0][j] + TENSOR_AGP[1][j]*TENSOR_AGP[1][j] + TENSOR_AGP[2][j]*TENSOR_AGP[2][j];
	    
	    /* debug */
            if(debugTensor == PSD_ON) {
                printf( "Shot = %d, X = %f, Y=%f, Z= %f\n", j, TENSOR_AGP[0][j], TENSOR_AGP[1][j], TENSOR_AGP[2][j] );
                fflush( stdout );
            }

        }

        fclose(fp); 

	    


    return SUCCESS;

}   /* end set_tensor_orientationsAGP() */

/* end @rsp */

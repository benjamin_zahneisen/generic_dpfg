/* *************************************
 * grad_rf_generic.h		1/30/2008 	ghg
 * This structure is used to track the 
 * rf heating, SAR heating, Grad coil heating,
 * grad amplifier heating.
 * ********************************** */

/* only do this once in any given compilation.*/
#ifndef  grad_rf_generic_INCL
#define  grad_rf_generic_INCL

/*declare dummy variables that hold pulse informations*/
int pw_rf1;
int ia_rf1;
float a_rf1;
int res_rf1;
int wg_rf1;
float flip_rf1;
int pw_gzrf1a;
int pw_gzrf1;
int pw_gzrf1d;
float a_gzrf1;

int pw_rf2;
float a_rf2;
float flip_rf2;
int res_rf2;
int wg_rf2;
int ia_rf2;

RF_PULSE rfpulse[MAX_RFPULSE] = {
  {(int *)&pw_rf1,
     (FLOAT *)&a_rf1, 
     SAR_ABS_SINC1,
     SAR_PSINC1,
     SAR_ASINC1,
     SAR_DTYCYC_SINC1,
     SAR_MAXPW_SINC1,
     1,
     MAX_B1_SINC1_90,
     MAX_INT_B1_SQ_SINC1_90,
     MAX_RMS_B1_SINC1_90,
     90.0,
     &flip_rf1,
     3200.0,
     1250.0,
     PSD_APS2_ON + PSD_MPS2_ON + PSD_SCAN_ON,
     0,
     0,
     1.0,
     (int *)&res_rf1,
     0
     , (int *)&wg_rf1
     },

{(int *)&pw_rf2,
     (FLOAT *)&a_rf2, 
     SAR_ABS_SINC1,
     SAR_PSINC1,
     SAR_ASINC1,
     SAR_DTYCYC_SINC1,
     SAR_MAXPW_SINC1,
     1,
     MAX_B1_SINC1_90,
     MAX_INT_B1_SQ_SINC1_90,
     MAX_RMS_B1_SINC1_90,
     90.0,
     &flip_rf2,
     3200.0,
     1250.0,
     PSD_APS2_ON + PSD_MPS2_ON + PSD_SCAN_ON,
     1,
     0,
     1.0,
     (int *)&res_rf2,
     0
     , (int *)&wg_rf2
     },
  
{(int *)&pw_rf2,
          (FLOAT *)&a_rf2,
          SAR_ABS_SINC1,
          SAR_PSINC1,
          SAR_ASINC1,
          SAR_DTYCYC_SINC1,
          SAR_MAXPW_SINC1,
          1,
          MAX_B1_SINC1_90,
          MAX_INT_B1_SQ_SINC1_90,
          MAX_RMS_B1_SINC1_90,
          90.0,
          &flip_rf2,
          3200.0,
          1250.0,
          PSD_APS2_ON + PSD_MPS2_ON + PSD_SCAN_ON,
          1,
          0,
          1.0,
          (int *)&res_rf2,
          0
          , (int *)&wg_rf2
          },

 /* {  (int *)&pw_rfwater,
     (float *)&a_rfwater, 
     SAR_ABS_SINC1,
     SAR_PSINC1,
     SAR_ASINC1,
     SAR_DTYCYC_SINC1,
     SAR_MAXPW_SINC1,
     1,
     MAX_B1_SINC1_90,
     MAX_INT_B1_SQ_SINC1_90,
     MAX_RMS_B1_SINC1_90,
     90.0,
     &ssfp_flip,
     3200.0,
     1250.0,
     PSD_APS2_ON + PSD_MPS2_ON + PSD_SCAN_ON,
     0,
     0,
     1.0,
     (int *)&res_rfwater,
     0
#if EPIC_RELEASE >= 15
     , (int *)&wg_rfwater
#endif
  },*/


#include "rf_Prescan.h"


};


/*#define MAX_ENTRY_POINTS 15*/
float maxB1[MAX_ENTRY_POINTS], maxB1Seq;



GRAD_PULSE gradx[MAX_GRADX] = {

  
};

GRAD_PULSE grady[MAX_GRADY] = {

};

GRAD_PULSE gradz[MAX_GRADZ] = {
  
};

#endif  

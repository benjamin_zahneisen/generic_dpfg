/*
 *  sbb_dw_dpfg.h
 *
 *  This file contains the prototypes declarations for all functions
 *  in sbb_dw_dpfg.e
 *  
 *  Language : ANSI C
 *  Author   : Benjamin Zahneisen
 *  Date     : Sep/2016
 *
 */

#ifndef sbb_dw_dpfg_h
#define sbb_dw_dpfg_h

#define sbb_dw_dpfg_DEBUG
/*
 * @global functions
 */


#include "mplx_rfpulse.h"
#include "sbb_diff_grad.h"


#define SSE 0
#define DSE 1
#define HSE 2
#define DTI 3
#define NO_DIF_GRAD -1

/*diffusion weighting sequence building block */
typedef struct sbb_dw_dpfg{

    
      sbb_refoc m_sbb180;
      mplx_pulse* prfpulse;

      sbb_diff_grad m_dgrad;
      sbb_diff_grad m_dgrad2;

      /*control variables */
      int Ninstances; /*how often do we employ that pulse per entry point */
      int mode; /*SSE,DSE,HSE*/
      float alpha; /*flip angle of 1st and 3rd rf-pulse in HSE-mode*/
      int antiparallel; /*enables anti-parallel gradient lobes*/
	
      /*timing*/
      int start_time; /*all other timing is relative to the sbb start time */
      int total_time; /*duration of sbb */
      int time_to_RF1; /*duration [microsec] from start to middle of 1st 180deg */
      int time_from_RF2; /*duration from middle of 2nd 180deg to end of sbb */
      int time_RF1_to_RF2;
      int tmix; /*duration between RF1 and RF2*/
      int tmixMin;
      int tfill1;
      int tfill2;
      int tdelta_lobes; /*extra delay time between gradient lobes in SSE mode */ 
      int moment1; /*gradient moment first diff gradient*/
      int moment2;
      float bval1;
      float bval2;
      int delta1;
      int delta2;
      int Delta1;
      int Delta2;
      int useMaxLogGrad; /*do we want to use full gradient strength ?*/
      int debug;

} sbb_dw_dpfg;


void initWF_PULSE(WF_PULSE* pwf);

/*
 * @host functions
 */
STATUS sbb_dw_dpfg_cveval(sbb_dw_dpfg* psbb,mplx_pulse* prf,float bval1,float bval2);
STATUS sbb_dw_dpfg_updateTE(sbb_dw_dpfg* psbb,int TE,int dTE );

STATUS sbb_dw_dpfg_predownload(void);
STATUS sbb_dw_dpfg_link_rfpulse(sbb_dw_dpfg* psbb,int slot);
/* functions */
/*int minte_for_bval(int *,int *, int *, int *, double *, int, double,double,double,double,double,double, int,int,int);*/
void SolveCubic(double,double,double,double,int *, double *);

/*
 * @pg functions
 */
STATUS sbb_dw_dpfg_pulsegen(sbb_dw_dpfg* psbb,CHAR* sbb_name,mplx_pulse* prf,int Nwaves);


/*************************/
/* @rsp functions */
/*************************/
void sbb_dw_dpfg_psdinit(sbb_dw_dpfg* psbb);
void sbb_dw_dpfg_setfrequency(sbb_dw_dpfg* psbb,long freq);
void sbb_dw_dpfg_setwave(sbb_dw_dpfg* psbb,int num);
void sbb_dw_dpfg_crusherTheme(sbb_dw_dpfg* psbb,int crushertheme);
int readAndSetDiffGradTable_AGP_dual(int fileIDNr,CHAR* fname_base, int mode);
void sbb_dw_dpfg_diffstep(sbb_dw_dpfg* psbb,float tensor[][MAX_DTI_DIRECTIONS + MAX_T2],int d);

#endif /*sbb_dw_dpfg_h */

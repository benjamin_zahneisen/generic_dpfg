/* generic.e
 * modified from gary's spiral sequence
 * Murat Aksoy aksoymurat@gmail.com
 /*Benjamin Zahneisen 2015
 * Stanford University, 2011
 */


@inline epic.h

@global
/*********************************************************************
 *                       generic_dPFG.E GLOBAL SECTION               *
 *                                                                   *
 * Common code shared between the Host and Tgt PSD processes.  This  *
 * section contains all the #define's, global variables and function *
 * declarations (prototypes).                                        *

/* System includes */
#include <string.h>
#include <stdio.h>

/*
 * EPIC header files
 */
#include "em_psd_ermes.in"
/*This is from epi2.e*/
#include "stddef_ep.h"
#include "epicconf.h"
#include "pulsegen.h"
#include "support_func.h"
#include "epic_error.h"
#include "epicfuns.h"
#include "psdutil.h"
#include "psd_proto.h"
#include "epic_iopt_util.h"
/*end of from epi2.e */

#include "filter_defs.h"  /*instead of "filter.h"*/
#include "grad_rf_generic.globals.h"
#include "ChemSat.h"

#define COMPILE_FOR_PSD 1

/*#include <InitAdvisories.h>*/
#include "../bz_common/generic_readouts/generic_ro.h"
#include "epic_iopt_util.h"

@inline debugfun.e DFlogmessage

#define NLMAX		128      /* maximum number of spiral interleaves*/
#define RES_GRAMP       100     /* number of points grad rampdown  */
#define TSP             2.0us   /* slow rcvr sampling 500kHz */
#define MAX_BW		250

#ifndef MAX
#define MAX(a,b) ( (a > b) ? a : b ) 
#endif
#ifndef MIN
#define MIN(a,b) ( (a < b) ? a : b ) 
#endif
#ifndef ABS
#define ABS(a) ( (a < 0) ? -a : a ) 
#endif
#ifndef LIMIT
#define LIMIT(x, xmin, xmax)   ( (x<xmax)? ((x>xmin)? x:xmin):xmax )
#endif
#define TWOPI 6.2831853

/* this is only used for spiral, not a big deal */
#define GRESMAX 50000

#define MAX_DTI_DIRECTIONS 256
/* pfile_pass_state : for multi pfiles */
#define KEEP_CURRENT    0
#define NEXT_PFILE      1
#define LAST_PFILE_DONE 2

@inline ChemSat.e ChemSatGlobal
@inline Prescan.e PSglobal


%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_global
%endif


/*include all sequence building blocks from ../bz_common*/
#include "../bz_common/rf_headers.h"
@inline ../bz_common/sbb_exc.e _global
@inline ../bz_common/sbb_180.e _global
@inline ../bz_common/sbb_nonSelective.e _global
@inline ../bz_common/sbb_diff_grad.e _global
@inline ../bz_common/generic_readouts/generic_ro.e _global
@inline ../bz_common/sbb_wait.e _global
@inline ../bz_common/sbb_spoil_grad.e _global
@inline ../bz_common/sbb_180.e _global

/*include local sequence building blocks (dervied from bz_common structures)*/
@inline sbb_dw_dpfg.e _global


@cv
@inline loadrheader.e rheadercv
@inline Prescan.e PScvs
@inline vmx.e SysCVs


int ni_cv = 1 with
{   1,1000000,,,"number of interleaves","ni"};
int ro_type_cv = 2 with
{   0,100,,,"readout type","ro_type"};
float gslew_cv = 200.0 with
{   0.0,1000.0,1,, "readout gradient max slew, mT/m/ms","gslew_cv"};
float gamp_cv = 3.1 with
{   0.0,10.0,1,, "readout gradient max amp","gamp"};
float gmax_cv = 3.1 with
{   0.0,10.0,1,, "bw-limited readout gradient amp","gmax"};
float gfov_cv = 20.0 with
{   0.0,100.0,1,, "readout gradient design FOV, cm","gfov"};
int epi_rampsamp_cv = 1 with
{   0,1,,,"Ramp Sampling Off (0) or On (1)","epi_rampsamp"};
int epi_blip_up_down_cv = 0 with
{   0,1,,,"Blip up and down for epi for dist corr","epi_blip_up_down_cv"};
int rewind_readout_cv = 1 with
{   0,1,,,"rewind readout? 0=no, 1=yes","rewind_readout"};
int nav_type_cv = -1 with
{   -1,14,,,"Navigator Type -1=none","nav_type"};
int reverse_readout_cv = 0 with
{   0,1,,,"reverse readout? 0=no, 1=yes","reverse_readout"};
/*int turnoff_readout_cv = 0 with {0,,,,"noreadout=1"};*/
float fn_cv = 1.0 with
{   0.0,,,,"fractional nex"};
float fn2_cv = 1.0 with
{   0.0,,,,"fractional nex in the second dim"};
int nz_cv = 32 with
{   0,,,,"z resolution"};
float fovz_cv = 128 with
{   0,,,,"z fov"};
int permute_cv = 12 with
{   0,,,,"permutations of readout"};
int R_cv = 1 with
{   0,,,,"real undersampling"};
int R_nav_cv = 1 with
{   0,,,,"real undersampling for nav"};
int R_skip_cv = 1 with
{   0,,,,"real undersampling"};
int epi_ref_cv = 0 with
{   0,,,,"add epi ref scan after excitation"};
/* ADD_NEW_RO_TYPE */

int n_nav_x = 6 with
{   0,512,,, "matrix size for navigator","n_nav_x"};
int n_nav_y = 6 with
{   0,512,,, "matrix 2nd size for navigator","n_nav_y"};
int n_nav_z = 1 with
{   0,512,,, "matrix 3rd size for navigator","n_nav_z"};
int ni_nav_cv = 1 with
{   1,512,,, "navigator number of interleaves","ni_nav_cv"};
int nav_pos = 3 with
{   0,3,,,"Navigator position 0=before 1=after imaging ro","nav_pos"};
int nextra = 0 with
{   0,,,,"number of disdaqs",};
int nframes = 1 with
{   1,,,,"number of time frames",};
int nframes_per_pfile = 1;
int total_views; /*  total shots to gather per slice */
int gating = TRIG_INTERN with
{   ,,,,"1=line,3=ecg,5=aux,7=intern,9=nline",};
int psdseqtime; /* sequence repetition time */
int timessi = 10000us with
{   0,,10000us,,"time from eos to ssi in intern trig",};
float revgzcrush = 4.0 with
{   0,,,, "number of revolutions for gz crusher",};
float areagzcrush = 0.0; /* area of the gz crushers around 180 */
int crushertheme = 0; /* the way the crushers are implemented */

int tlead = 10000us; /* initial lead time for rf pulse prep*/
int cblank = 20ms; /* cardiac blanking time after last seq w/ ecg trigger */
float daqdeloff = 0.0us; /* fudge factor, tuned at SRI  */
int daqdel = 128us; /*  gradient delay vs readout */
int startwait = 0; /*start time of WAIT gradient pulse */
int maxWait = 2000; /*max time of WAIT pulse*/
/* sliding data acquisition window control: */
int   tfon = 1 with {0,1,1,VIS,"Time shift interleaves:0=off,1=on.",};
/*int daqdel2 = 128us;*//*murat*/
int thetdeloff = -6us;
int thetdeloff2 = 0us;
int echoshift = 0us with
{   ,,,,"180 advance for spin echo T2* wting","echoshift"};
/* echo spacing is needed because tehre is an error if the echoes are right next to each other */
int espace = 16us with
{   0,,1,, "space between echoes","espace"};
float gmax_factor = 1.0 with
{   0,,1.0,, "multiply maximum gradient and slew rate by this number","gmax_factor"};

int pass_multiple_pfiles = 0; /* pass multiple files 0=no 1=yes */
int pos_pass_pulse;        /* position of pass pulse */	
int pfile_pass_state  = 0; /* switches to new pfile if needed */

int filter_echo_nav;
int filter_echo;
int filter_echo_epi_ref;
int opnecho_imag = 1; /*number of echoes of the imaging ro */

/* fat sat vars */
int fatsat = 0;
int   fStrength;        /* FEC : Field strength temp variable for RF calculations */
int cs_isodelay;
int pw_rfcssat;
float satBW = 440.0 with
{   0.0,1000.0,1,, "sat bw, Hz",};
float satoff = -520.0 with
{   -1000.0,1000.0,1,, "sat center freq, Hz",};
int slice_order = 0 with {   ,,,, "Slice ordering 0=linear,1=interleaved [0 2 4 ... 1 3 5...]",};
float slwid180 = 1.1 with
{   0.0,4.0,,,"180 slice width as fraction of 90",}; /*test on Oct 09, 2017. Found 1.1 reduces steady-state artifact at SMS-group borders */
int first180_pol = -1 with {-1,1,,,"polarity of 180",}; /* polarity of first 180 */
int second180_pol = 1; /* polarity of second 180, 1 or zero please */
int alternate_180 = 0; /* switch polarity of second 180 */

/*run fieldmap scans*/
int domap = 0 with
{   0,1,0,, "1=do B0 map ",};
int mapdel = 2ms; /* variable delay for field map */
int bmapnav = 1 with
{   0,1,1,, "1=do nav cor for bmap",};
int off_fov = 0 with
{   0,1,1,, "1 for rcvr offset fov, 0 for fftshift",};
float tsp = 4.0us with
{   1.0,12.75,,, "A/D sampling interval",};
float bandwidth = 125.0 with
{   2.0,250.0,1,, "CERD rec low pass freq, kHz",};
float decimation = 1.0;

int filter_aps2 = 1;

int psfrsize = 256 with
{   0,,,, "num data acq during APS2",};
int queue_size = 1024 with
{   0,,,, "Rsp queue size",};

int nbang = 0 with
{   0,,,,"total rf shots",};
float zref = 1.0 with
{   ,,,, "refocussing pulse amp factor",};

int trigloc = 5ms with
{   0,,0,, "location of ext trig pulse after RF",};
int triglen = 5ms with
{   0,,0,, "duration of ext trig pulse",};
int trigfreq = 1 with
{   1,128,0,, "num views between output trigs",};
int maketrig = 0 with
{   0,1,0,, "trigger output port (1) or not (0)",};

int numdda = 4; /* For Prescan: # of disdaqs ps2*/
int pos_start = 0 with
{   0,,,, "Start time for sequence. ",};
int pos_start_rf = 0 with
{   0,,,, "Start time for 1st rf. ",};

int obl_debug = 0; /* for obloptimize, I guess */
int obl_method = 0 with
{   0,1,0,,
    "On(=1) to optimize the targets based on actual rotation matrices",};
int debugstate = 1;

int zone_cntl = 1 with
{   0,2,0,, "ACGD zone control: 0=none, 1=pre-GP3, 2=GP3 ",};
int acgd_tr = 400ms;
int nimages; /* total number acquired */

int turnoff_diff = 0; /* no diff grads */

int multiecho_180_bitmask = 65535 with
{   0,65535,,, "whether 180 or no 180 is used between echoes",};
/* some additional explanation about multiecho_180_bitmask here:
 * if you have 5 imaging echoes (no navigator), and you want a 180 only after the 3rd echo,
 * set this number to 
 * 0 0 1 0
 * which is 2. 
 * only the least significant bits are considered. 
 * bitmak is only for the imaging echo. The navigator echo is assumed to have a single
 * echo and the refocusing is controlled by nav_pos
 */


/*BZ multi-band cvs*/
int mb_factor = 1 with
{   1,MAX_MPLX_SLICES,1,VIS,"Number of simultaneously excited slices",};
float mb_acceleration = 1 with
    {   1,MAX_MPLX_SLICES,1,VIS,"Multi-band acceleration factor",};
int optimSAR = 1 with {0,1,1,VIS,"SAR reduction by phase modulation on/off",};
float flipRF2 = 180 with {90,180,1,VIS,"Flip angle refocussing pulse",};
float stretchRF = 2.0 with {0.0,4.0,1,VIS,"Stretch RF factor",};
int debugMplx = 1 with {0,1,1,VIS,"debug output for multi-band rf",};
int caipi_blips_cv = 0 with {   0,1,1,VIS,"z-gradient blips 0/1 = off/on",};
int mb_rfmod_cv = 0 with {   0,1,1,VIS,"RF phase modulation for mplx encoding 0/1 = off/on",};
int pomp_calib_cv = 0 with {   0,1,1,VIS,"RF phase modulation for SENSE calibration 0/1 = off/on",};
int mb_single_calib_cv = 1 with {   0,1,1,VIS,"RF single band calibration 0/1 = off/on",};
int mb_single_calib_blip_down_cv = 1 with {   0,1,1,VIS,"RF single band calibration blip down 0/1 = off/on",};
int K0_Idx = 0; /*index of pomp rf-pulse array which corresponds to Kpomp = 0*/
float ro15_te = 0.0 with {0.0,1.0,0.05,VIS,"where to put the ro15 echo time",};
int data_interleaves = 1 with {   0,99,1,VIS,"how many interleaves are acquired",};
/*int rfov = 0 with {0,1,1,VIS,"reduced fov ?"};*/
float myrloc = 5 with {0,,,INVIS, "Value for scan_info[0].oprloc",};

/*BZ dpfg */
int bval1_cv = 100 with {   0,4000,1,VIS,"b-value for 1st gradient pair",};
int bval2_cv = 100 with {   0,4000,1,VIS,"b-value for 2nd gradient pair",};
int dpfgMode_cv = 1 with {   0,3,1,VIS,"DPFG mode: SSE/DSE/HSE/DTI",};
int minte;
int tefill1 = 0;
int tefill2 = 0;
float deltaTE_cv = 0 with {   0,200.0,0.5,VIS,"deltaTE [ms]",};
int deltaTE_psd = 0; /*delta TE in [microsec]*/
int fileId_cv = 0 with{  -1,99,1,VIS,"which gradient table _00, _01",};
int antiparallel_cv = 0 with{  0,1,1,VIS,"enable anti-parallel mode ",};

/*from diff_fun.e*/
float  	max_g_norm 	= 1.0;
int    	use_maxloggrad 	= 1 	with {0,1,1,VIS,"use maxgrad for dwi traditional sampling scheme",}; /* Stefan Skare changed to 1 by default. This variable is used differently now */
int    	early_1st_diffgrad = 1 	with {0,1,1,VIS,"1: Just after 90. 0: Just before 1st 180",};
int 	debugTensor 	= 0  	with {0,1,0,VIS,"Tensor Debugging Flag",};
int 	num_tensor 	= 0 	with {0,MAX_DTI_DIRECTIONS,7,VIS,"Number of Diffusion Directions",};
int 	num_B0 		= 2 	with {0,20,5,VIS,"Number of B = 0 Images",};
int     num_dif_vol	= 1 	with {0,MAX_DTI_DIRECTIONS+20,5,VIS,"Number Diffusion volumes",};
int orthoDiffDir = 0 with {0,1,1,VIS,"flag for overwriting AGP_DIFF with X/Y/Z-dir only",};

/* for minte_for_bval define these before minte_for_bval calc*/
float 	RFiso2end	= 0;	/* RF isocenter */
float 	time2echo   	= 0; 	/* the time from the beginning of the readout to the echo. */

float min_phasefov = 0.5 with {0.1,1.0,0.5, INVIS, "Minimum phase FOV holder",};
int   td0 = 4; /*cardiac trigger delay */
float scalefovy = 1.2 with {1.0,2.0,0.05,VIS,"scale phase encoding fov",};

/* Mahdi - add trigger delay sweep functionality */
int sweep_td = 1 with {0,1, 0, VIS, "do trigger delay sweep= 1, don't = 0", ""};
int sweep_step = 100000 with {0,100000, 0, VIS, "trigger delay sweep in microseconds", ""};
int sweep_Nsteps = 10 with {1,20,0,VIS, "number of different cardiac trigger delays in sweep",""};
int ctriggerMode = 0 with{0,1,0,VIS,"cardiac trigger mode",""};
int deadTime = 1;
int act_tr    = 0 with {0,,,INVIS, "actual tr",};


/* WWN: RF calculations */
float ave_sar;
float avecoil_sar;
float peak_sar;
float max_seqsar;



%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_cv
%endif

@ipgexport
@inline Prescan.e PSipgexport

RF_PULSE_INFO rfpulseInfo[RF_FREE];

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_ipgexport
%endif

/*THE generic readout */
generic_ro w;
generic_ro w_nav;
generic_ro w_epi_ref; /* to be used only with epi ref scan */
time_t lasttime = 0;
int multiecho_180_bitmask_arr[16]; /* generated from multiecho_180_bitmask cv */


/*BZ multi-band excitation */
#include "../bz_common/mplx_rfpulse.h"
mplx_pulse mplxRF1[MAX_MPLX_SLICES]; /*array of excitation pulses; e.g stores single and multiband pulses*/
mplx_pulse mplxRF2[MAX_MPLX_SLICES];
mplx_pulse fatSatRF = {0};
float Kpomp[MAX_MPLX_SLICES];


char seq_header_name[100];

float dummyScanInfo[8];

/*define sequence building blocks (sbb) here. Each block tries to act as an independent sequence component*/
sbb_exc 		excSBB = {0}; /*some kind of initialization*/
sbb_refoc 		SBB180 = {0};
sbb_nonSelective 	FatSatSBB = {0};
sbb_dw_dpfg 		dpfgSBB = {{0}};
sbb_spoil_grad		spoilSBB = {0};

/******************/
/*HOST section    */
/******************/
@host
#include "sar_pm.h"

/* System includes from epi2 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h> /*RTB0 correction*/
#include <ctype.h>
#include <math.h>
/*end of epi2*/

#include "grad_rf_generic.h"
/*DV26 */
%ifdef DV26
#include "psdIF.h"
#include "psdopt.h"
%endif

static char supfailfmt[] = "Support routine %s exploded!";

FILTER_INFO echo_filt;
FILTER_INFO echo_filt_nav;
FILTER_INFO echo_filt_epi_ref;

@inline Prescan.e PShostVars

%ifdef DV26
@inline loadrheader.e rheaderhost /*DV26*/
%endif

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_host_fundef
%endif

/*BZ multiband excitation */
#include "../bz_common/generic_readouts/generic_ro.c"
#include "../bz_common/helper_funcs.h"
#include "../bz_common/mplx_rfpulse.c"
@inline ../bz_common/sbb_exc.e _host
@inline ../bz_common/sbb_180.e _host
@inline ../bz_common/sbb_nonSelective.e _host
@inline ../bz_common/sbb_diff_grad.e _host
@inline ../bz_common/sbb_wait.e _host
@inline ../bz_common/sbb_spoil_grad.e _host

/*local building blocks */
@inline sbb_dw_dpfg.e _host

/* ****************************************
   MYSCAN
   myscan sets up the scan_info table for a hypothetical scan.
   It is controlled by the cv opslquant, and opslthick, and opfov. 
   ************************************** */
void
myscan( void )
{
    int i,j;
    int num_slice;
    float z_delta;		/* change in z_loc between slices */
    float r_delta;		/* change in r_loc between slices */
    double alpha, beta, gamma; /* rotation angles about x, y, z respectively */
    
    num_slice = exist(opslquant);
    
    r_delta = exist(opfov)/num_slice;
    z_delta = exist(opslthick)+exist(opslspace);
    
    scan_info[0].optloc = 0.5*z_delta*(num_slice-1);
    if (myrloc!=0.0)
        scan_info[0].oprloc = myrloc;
    else 
        scan_info[0].oprloc = 0;
    
    for (i=1;i<9;i++)
        scan_info[0].oprot[i]=0.0;
    
    switch (exist(opplane)) {
    case PSD_AXIAL:
        scan_info[0].oprot[0] = 1.0;
        scan_info[0].oprot[4] = 1.0;
        scan_info[0].oprot[8] = 1.0;
        break;
    case PSD_SAG:
        scan_info[0].oprot[2] = 1.0;
        scan_info[0].oprot[4] = 1.0;
        scan_info[0].oprot[6] = 1.0;
        break;
    case PSD_COR:
        scan_info[0].oprot[1] = 1.0;
        scan_info[0].oprot[5] = 1.0;
        scan_info[0].oprot[6] = 1.0;
        break;
    case PSD_OBL:
	fprintf(stderr,"oblique \n");
        alpha = PI/4.0;  /* rotation about x (applied first) */
        beta = PI/8.0;   /* rotation about y (applied 2nd) */
        gamma = PI/4.0;  /* rotation about z (applied 3rd) */
        scan_info[0].oprot[0] = cos(gamma)*cos(beta);
        scan_info[0].oprot[1] = cos(gamma)*sin(beta)*sin(alpha) -
                                       sin(gamma)*cos(alpha);
        scan_info[0].oprot[2] = cos(gamma)*sin(beta)*cos(alpha) +
                                       sin(gamma)*sin(alpha);
        scan_info[0].oprot[3] = sin(gamma)*cos(beta);
        scan_info[0].oprot[4] = sin(gamma)*sin(beta)*sin(alpha) +
                                       cos(gamma)*cos(alpha);
        scan_info[0].oprot[5] = sin(gamma)*sin(beta)*cos(alpha) -
                                       cos(gamma)*sin(alpha);
        scan_info[0].oprot[6] = -sin(beta);
        scan_info[0].oprot[7] = cos(beta)*sin(alpha);
        scan_info[0].oprot[8] = cos(beta)*cos(alpha);
        break;
    }
  
    for(i=1;i<num_slice;i++) {
        scan_info[i].optloc = scan_info[i-1].optloc - z_delta;
        scan_info[i].oprloc = i*r_delta;
        for(j=0;j<9;j++)
            scan_info[i].oprot[j] = scan_info[0].oprot[j];
    }
    
    return;
    
}


/*Set the sequence name */
abstract("generic sequence");
psdname("generic_dPFG");

int cvinit()
{

    EpicConf();

%ifdef RTMOCO
    psd_targetscale=0.9;
%endif

    inittargets(&loggrd, &phygrd);
    if( obloptimize(&loggrd, &phygrd, scan_info, exist(opslquant), exist(opplane), exist(opcoax), obl_method, obl_debug,
                    &opnewgeo, cfsrmode)
        == FAILURE)
        return FAILURE;

    if( debugstate )
        write_log("cvinit\n");


@inline Prescan.e PScvinit

#include "cvinit.in"	/* Runs the code generated by macros in preproc.*/


    /* for most of the generic we want one pfile */
    pass_multiple_pfiles = 0;

    /*BZ tr,te,resolution*/
    cvmax(optr, 24s);
    cvmax(ihtr, 24s);
    cvmin(opxres,64);
    cvdef(opxres,64);
    cvmin(opyres,64);
    cvdef(opyres,64);
    cvdef(opautote,2);

    /* sequence type*/
    cvdef(oppseq, 2);
    cvmin(oppseq, 0);
    cvmax(oppseq, 4);

    /* slice thickness - rafael */
    cvmax(opslthick, 300);

    /* slice overlap can be negative because this can be 3D as well */
    cvoverride(pioverlap, 1, 1, 1);

    /* nex for partial k-space*/
    pinexnub = use5 - 1;
    pinexval2 = 1;
    pinexval3 = 0.75;
    pinexval4 = 0.6;
    pinexval5 = 0.5;

    /* turn on variable bandwidth button */
    pidefrbw = 125.0;
    pircbnub = 3; /* number of variable bandwidth buttons */
    pircbval2 = 166.0; 
    pircbval3 = 250.0;
    pircbval4 = 125.0;
    cvmin(oprbw, 15.875);
    cvmax(oprbw, 250.0);
    cvdef(oprbw, 125.0);       
    oprbw = 125.0; /* MGD: +/- 250 Digital */

    /* RBW UI Choices */
    pircb2nub = 0;    /* turn off 2nd echo rbw buttons */

    cvmin(opxres, 16);
    cvmin(opyres, 16);
    
    /*slice order button*/
    pisliceordnub = 1;


   /* straighten out some variables */
   cvmax(opdiffuse,	PSD_ON);
   cvmax(opepi,		PSD_ON);
   cvmax(opbval,	4000);
   
   /* BZ we want to have at least 2 t2 images  */
   cvmin(opdifnumt2,    1); 
   cvunlock(opdifnumt2);
   cvdef(opdifnumt2,2);
   
   /* unlock some things */
   cvunlock(opdiffuse	);
   cvunlock(opepi	);
   cvunlock(optensor	);

   /* turn on diffusion page : note pidifpage does nothing*/
   if(opdiffuse==PSD_OFF) {
 	
   	cvoverride(optensor, 0, PSD_FIX_OFF,PSD_EXIST_ON);
   	cvoverride(opepi,    0, PSD_FIX_OFF,PSD_EXIST_ON);
   }

    pinumbnub  = 0;     /* turns off the number of bvals button  */
    pidifnext2nub = 0;  /* turns off the t2 nex button 

    /* these let you change the bval in the table to control opbval (see below) */
    avminbvalstab = 0;
    avmaxbvalstab = 4000;

    /* sets the bvalue to be the top one in the table - 
    if you turned the buttons above off there will only be
    one */
    opbval = bvalstab[0];

    opdifnext2 = difnextab[0];

    pibvalstab = 0; /* show bvalue table */
    pidifnextab = 1; /* show NEX table */
    avmindifnextab = 1;
    avmaxdifnextab = 20;

    pidualspinechonub = 0;
    cvoverride(opdualspinecho,    0, PSD_FIX_OFF,PSD_EXIST_ON);

    /*cardiac specific fields*/
    if(opcgate == PSD_ON){
	pitseqnub = 0; /* Bitmask for Inter-Sequence Delay buttons */
	pitseqtype = PSD_LABEL_INTERSEQDELAY_DEFAULT;  /* text label for inter- sequence delay button */

	cvunlock(phys_record_flag);
	/*cvoverride(phys_record_flag,    1, PSD_FIX_OFF,PSD_EXIST_ON);*/
	phys_record_flag = 1;
	phys_record_channelsel = 240;
    }


    /*initialize SBBs */
    sbb_exc_init(&excSBB);
    sbb_refoc_init(&SBB180);
    sbb_nonSelective_init(&FatSatSBB);
    multiplexPulse_init(&fatSatRF);
    int n;
    for(n=0;n<MAX_MPLX_SLICES;n++){
        multiplexPulse_init(&mplxRF1[n]);
        multiplexPulse_init(&mplxRF2[n]);
    }
    sbb_dw_dpfg_init(&dpfgSBB);
    sbb_spoil_grad_init(&spoilSBB);	
  
    if( debugstate )
        write_log("\tend\n");

    return SUCCESS;

}


/*@inline InitAdvisories.e InitAdvPnlCVs*/

int cveval()
{
    /*InitAdvPnlCVs();*/
    int tmptr;
    int i;
    float max_rbw;

    write_log("cveval start\n");
	

    if( _psd_rf_wait.fixedflag == 0 )
    { /* sets psd_grd_wait and psd_rf_wait */
        if( setsysparms() == FAILURE)
        {
            epic_error(use_ermes, "Support routine setsysparams failed", EM_PSD_SUPPORT_FAILURE, 1, STRING_ARG,
                       "setsysparms");
            return FAILURE;
        }
    }

    if( obloptimize(&loggrd, &phygrd, scan_info, exist(opslquant), exist(opplane), exist(opcoax), obl_method, obl_debug,
                    &opnewgeo, cfsrmode)
        == FAILURE)
        return FAILURE;

    if( existcv(opcgate) && (opcgate == PSD_ON) )

    {
        tmptr = RUP_GRD((int)((float)(exist(ophrep))
                *(60.0/exist(ophrate))* 1e6));
        pitrnub = 0;
    }
    else
    {
        tmptr = optr;
        pitrnub = 6;
        if( oppseq == PSD_GE ) /* gradient echo */
        {
            pitrval2 = 80ms;
            pitrval3 = 250ms;
            pitrval4 = 500ms;
            pitrval5 = 1s;
            pitrval6 = 2s;
        }
        else
        {
            pitrval2 = 300ms;
            pitrval3 = 500ms;
            pitrval4 = 1s;
            pitrval5 = 1.5s;
            pitrval6 = 2s;
        }
    }

    piisnub = 1; /* spacing */
    opovl = 1; /*overlap locs */
    pistnub = 1; /* slice thickness */
    pislqnub = 0; /*number of locations per slab */
    pilocnub = 0;
    epi_ref_cv = 0;


    if( opnecho_imag != 0 )
    {
        pisctim1 = (nframes * w.ni / opnecho_imag) * tmptr;
        pisctim2 = pinexval2 * pisctim1;
        pisctim3 = pinexval3 * pisctim1;
        pisctim4 = pinexval4 * pisctim1;
        pisctim5 = pinexval5 * pisctim1;
        pisctim6 = pinexval6 * pisctim1;
    }

    opflip = 20.0;
    cvdef(opflip, 20.0);
    if( oppseq == PSD_GE ) /* gradient echo */
    {
        pifanub = 6;
        pifaval2 = 10;
        pifaval3 = 30;
        pifaval4 = 40;
        pifaval5 = 60;
        pifaval6 = 90;
    }
    else /* spin echo */
    {
        pifanub = 1; /* turn off flip angle buttons */
	pifaval2 = 90.0;
    }

    /* label TE buttons */
    pite1nub = 63; /* apparently a bit mask */
    pite1val2 = PSD_MINIMUMTE;
    pite1val3 = 20ms;
    pite1val4 = 30ms;
    pite1val5 = 40ms;
    pite1val6 = 50ms;

    /* label FOV buttons */
    cvdef(opfov, 200);
    opfov = 200;
    pifovnub = 6;
    pifovval2 = 200;
    pifovval3 = 220;
    pifovval4 = 240;
    pifovval5 = 360;
    pifovval6 = 480;

    /* phase fov */
    cvdef(opphasefov, 1.0);
    piphasfovnub = 6;
    piphasfovval2 = 1.0;
    piphasfovval3 = 0.8;
    piphasfovval4 = 0.6;
    piphasfovval5 = 0.4;
    piphasfovval6 = 0.2;

    /* label resolution buttons*/
    piamnub = 7;
    pixresnub = 3;
    cvmin(opxres, 16);
    pixresval2 = 64;
    pixresval3 = 96;
    pixresval4 = 128;

    piyresnub = 3;
    cvmin(opyres, 16);
    piyresval2 = 64;
    piyresval3 = 96;
    piyresval4 = 128;

    /* open number of echoes button */
    piechnub = 1 + 2 + 4 + 8 + 16 + 32;
    piechdefval = 1;
    piechval2 = 1 + (nav_type_cv > -1) + epi_ref_cv;
    piechval3 = 2 + (nav_type_cv > -1) + epi_ref_cv;
    piechval4 = 4 + (nav_type_cv > -1) + epi_ref_cv;
    piechval5 = 8 + (nav_type_cv > -1) + epi_ref_cv;
    piechval6 = 16 + (nav_type_cv > -1) + epi_ref_cv;
    cvmin(opnecho, 1 + (nav_type_cv > -1) + epi_ref_cv);
    cvdef(opnecho, 1 + (nav_type_cv > -1) + epi_ref_cv);

    cvoverride(opautonecho, 0, 1, 1); /* I dont know what this is useful for */


    /*!!!! fill this up */
    /* turn on rhuser cv page */
    piuset = use0 + use1 + use2 + use3 + use4 + use5 + use6 + use7 + use8 + use9 + use10 + use11 + use12 + use13 + use14
             + use15 + use16 + use17 + use18 + use19 + use20 + use21 + use22 + use23;
    pititle = 1;
    cvdesc(pititle, "^<>^<> Welcome To Generic <>^<>^");

    cvdesc(opuser0, "Readout type");
    cvdef(opuser0, ro_type_cv);
    opuser0 = ro_type_cv;
    cvmin(opuser0, 0);
    cvmax(opuser0, 9999999);
    ro_type_cv = opuser0;

    cvdesc(opuser1, "k-space segments (in-plane)");
    cvdef(opuser1, 1.0);
    opuser1 = 1;
    cvmin(opuser1, 1.0);
    cvmax(opuser1, 9999999);
    ni_cv = opuser1;
    
    cvdesc(opuser2, "in-plane acceleration Ry");
    cvdef(opuser2, 1);
    opuser2 = 1;
    cvmin(opuser2, 1);
    cvmax(opuser2, 8);
    R_cv = opuser2;
    
    cvdesc(opuser3, "fractional nex");
    cvdef(opuser3, 0.75);
    opuser3 = 0.75;
    cvmin(opuser3, 0);
    cvmax(opuser3, 1.0);
    fn_cv = opuser3;
    
    cvdesc(opuser4, "multi-band factor");
    cvdef(opuser4, 1);
    opuser4 = 1;
    cvmin(opuser4, 1);
    cvmax(opuser4, 8);
    mb_factor = opuser4;

    cvdesc(opuser5, "CAIPI-factor");
    cvdef(opuser5, 1);
    opuser5 = 1;
    cvmin(opuser5, 1);
    cvmax(opuser5, 8);
    mb_acceleration = opuser5;
    
    cvdesc(opuser6, "Slice mode linear (0) or interleaved (1)");
    cvdef(opuser6, 1);
    opuser6 = 1;
    cvmin(opuser6, 0);
    cvmax(opuser6, 1);
    opslice_order = opuser6;

    cvdesc(opuser7, "Blip Up Down ");
    cvdef(opuser7, 0);
    opuser7 = 0;
    cvmin(opuser7, 0);
    cvmax(opuser7, 1);
    epi_blip_up_down_cv = opuser7;

    cvdesc(opuser8, "FOVz");
    cvdef(opuser8, fovz_cv);
    opuser8 = fovz_cv;
    cvmin(opuser8, 0);
    cvmax(opuser8, 300);
    fovz_cv = opuser8;
    cvoverride(fovz_cv, opuser8, PSD_FIX_ON, PSD_EXIST_ON);
    
    cvdesc(opuser9, "z resolution for 3D");
    cvdef(opuser9, 1.0);
    opuser9 = 1.0;
    cvmin(opuser9, 1);
    cvmax(opuser9, 512);
    nz_cv = opuser9;

    cvdesc(opuser10, "fractional nex 2");
    cvdef(opuser10, 1.0);
    opuser10 = 1.0;
    cvmin(opuser10, 0.5);
    cvmax(opuser10, 1.0);
    fn2_cv = opuser10;
    cvoverride(fn2_cv, opuser10, PSD_FIX_ON, PSD_EXIST_ON);

    cvdesc(opuser11, "number of temporal frames");
    cvdef(opuser11, 1);
    opuser11 = 1;
    cvmin(opuser11, 1);
    cvmax(opuser11, 16384);
    nframes = opuser11;

    cvdesc(opuser12, "Number of extra shots before data acq");
    cvdef(opuser12, 0);
    opuser12 = 0;
    cvmin(opuser12, 0);
    cvmax(opuser12, 100);
    nextra = opuser12;
  
    cvdesc(opuser13, "DPFG mode 0=SSE, 1=DSE, 2=HSE, 3=DTI");
    cvdef(opuser13, 3);
    opuser13 = 3;
    cvmin(opuser13, 0);
    cvmax(opuser13, 3);
    dpfgMode_cv = opuser13;
    
    cvdesc(opuser14, "deltaTE for DPFG (TE = minTE + deltaTE)");
    cvdef(opuser14, 0.0);
    opuser14 = 0.0;
    cvmin(opuser14, 0.0);
    cvmax(opuser14, 9999999.0);
    deltaTE_cv = opuser14;

    cvdesc(opuser15, "b-value of first diffusion gradient");
    cvdef(opuser15, 100);
    opuser15 = 100.0;
    cvmin(opuser15, 10.0);
    cvmax(opuser15, 10000.0);
    bval1_cv = opuser15;
    cvoverride(bval1_cv, opuser15, PSD_FIX_ON, PSD_EXIST_ON);

    cvdesc(opuser16, "b-value of 2nd diffusion gradient");
    cvdef(opuser16, 200);
    opuser16 = 100.0;
    cvmin(opuser16, 10.0);
    cvmax(opuser16, 10000.0);
    bval2_cv = opuser16;
    cvoverride(bval2_cv, opuser16, PSD_FIX_ON, PSD_EXIST_ON);
    
    cvdesc(opuser17, "fileID selects gradient table, (-1) = tensor.dat");
    cvdef(opuser17, -1);
    opuser17 = -1;
    cvmin(opuser17, -1);
    cvmax(opuser17, 99);
    fileId_cv = opuser17;

    cvdesc(opuser18, "navigator type, -1=none, ...");
    cvdef(opuser18, -1);
    opuser18 = -1;
    cvmin(opuser18, -1);
    cvmax(opuser18, 14);
    nav_type_cv = opuser18;
    cvoverride(nav_type_cv, opuser18, PSD_FIX_ON, PSD_EXIST_ON);

    cvdesc(opuser19, "navigator position, 0=before 1=after imaging ro ...");
    cvdef(opuser19, 3);
    opuser19 = 3;
    cvmin(opuser19, 0);
    cvmax(opuser19, 3);
    nav_pos = opuser19;
    cvoverride(nav_pos, opuser19, PSD_FIX_ON, PSD_EXIST_ON);

    cvdesc(opuser20, "antiparallel DPFG encoding (1)");
    cvdef(opuser20, 0.0);
    opuser20 = 0.0;
    cvmin(opuser20, 0.0);
    cvmax(opuser20, 1.0);
    antiparallel_cv = opuser20;
    cvoverride(antiparallel_cv, opuser20, PSD_FIX_ON, PSD_EXIST_ON);

    cvdesc(opuser21, "free param 2");
    cvdef(opuser21, 6);
    opuser21 = 6;
    cvmin(opuser21, 0);
    cvmax(opuser21, 9999999.0);
    /*n_nav_x = opuser21;*/

    cvdesc(opuser22, "free param 3");
    cvdef(opuser22, 6);
    opuser22 = 6;
    cvmin(opuser22, 0);
    cvmax(opuser22, 9999999.0);
    /*n_nav_y = opuser22;*/

    cvdesc(opuser23, "free param 4");
    cvdef(opuser23, 1);
    opuser23 = 1;
    cvmin(opuser23, 0);
    cvmax(opuser23, 9999999.0);
    /*n_nav_z = opuser23;*/
    /*ADD_NEW_RO_TYPE*/

    areagzcrush = 2 * revgzcrush * 10000000.0 / GAM / (float)opslthick;

    write_log("before fatsat\n");

    /*-------------------------------------------------------*/
    /* BZ Fat Saturation pulse                               */
    /*-------------------------------------------------------*/
    /*initialize single band wave*/
    multiplexPulse_initWaveFromFile(&fatSatRF,"rfcs3t.rho","none", RES_RFCS3T);

    /*from ChemSat.e*/
    fStrength=cffield;
    pw_rfcssat  = RNEAREST_RF((int)(fStrength*16ms),RES_RFCS3T*4);
    cs_isodelay = RDN_RF((pw_rfcssat >> 1));        /* Divide by 2 */

    write_log("cveval after fat init\n");

    satoff = SDL_GetChemicalShift(cffield);
 
    multiplexPulse_initWaveParams(&fatSatRF,SAR_CS3T_MAX_B1,-1,SAR_CS3T_NOM_FLIP,SAR_CS3T_NOM_PW,RES_RFCS3T);

    multiplexPulse_rfstat(&fatSatRF);

    /*multiplexPulse_print(&fatSatRF);*/

    /*link pulse and excitation gradients */
    fatSatRF.alpha = 95;

    multiplexPulse_link_rfpulse(&fatSatRF,rfpulse,RFFATWATER_SLOT);
    rfpulse[RFFATWATER_SLOT].num = 1;
    rfpulse[RFFATWATER_SLOT].max_int_b1_sq = SAR_CS3T_MAX_INT_B1_SQ;
    rfpulse[RFFATWATER_SLOT].max_rms_b1 = SAR_CS3T_MAX_RMS_B1;

    /*FatSatSBB.prfpulse = &fatSatRF;
    sbb_nonSelective_cveval(&FatSatSBB);*/

    /*-------------------------------------------------------*/
    /* BZ prepare multiplex excitation                       */
    /*-------------------------------------------------------*/
    #ifdef SIM
      myscan();	
      /*scandump();*/
	for(i=0;i<opslquant;i++){ 
	fprintf(stderr,"scan_info.tloc = %f \n",scan_info[i].optloc);
	}
    #endif

	write_log("cveval before exc init init\n");

    /*initialize single band wave*/
    multiplexPulse_initWaveFromFile(&mplxRF1[0],"bz_sinc2.rho","none", NOM_RES_RFEXT_SINC2);

    write_log("cveval after init\n");

    /*set parameters of rf pulse*/
    sprintf(mplxRF1[0].name,"mplxPulse");
    mplxRF1[0].useWongPhases = optimSAR;

    multiplexPulse_initWaveParams(&mplxRF1[0],MAX_B1_RFEXT_90_SINC2,NOM_BW_RFEXT_SINC2,NOM_FA_RFEXT_SINC2,NOM_PW_RFEXT_SINC2,NOM_RES_RFEXT_SINC2);

    /*multiplexPulse_stretchWave(&mplxRF1[0],stretchRF);*/

    /*set slice selection amplitude based on pulse bandwidth*/
    if (ampslice(&mplxRF1[0].slcSelectAmp,mplxRF1[0].bw,opslthick,1.0, TYPDEF) == FAILURE) {return FAILURE;}

    /*get multi-band slice locations based on scan geometry*/
    multiplex_sliceLocs(&mplxRF1[0],scan_info,opslquant,mb_factor);

    /*zero phase modulation for first rf pulse*/
    multiplexPulse_modulate(&mplxRF1[0],mb_factor,0.0,0.0);
    
	multiplexPulse_export(&mplxRF1[0]);

    /*calculate SAR relevant parameters*/
    multiplexPulse_rfstat(&mplxRF1[0]);

    /*display pulse parameters*/
    if(debugstate)
        multiplexPulse_print(&mplxRF1[0]);

    /*link pulse with user input and excitation gradients */
    mplxRF1[0].alpha = opflip;
    mplxRF1[0].a_RF = mplxRF1[0].alpha/mplxRF1[0].nom_alpha;

    /*init all pomp pulses with properties from zero-phase pulse even if no pomp is needed */
    multiplexPulse_initPompArray(&mplxRF1[0],mplxRF1,mb_factor+1); /*We need a single band pulse at mplxRF1[mb_factor]*/

    /*----------------------------------------------------------------*/
    /*BZ pomp encoding for excitation pulse (if mb_factor > 1)        */
    /*----------------------------------------------------------------*/
    if(mb_rfmod_cv || pomp_calib_cv){

        /*get a full k-space*/
        K0_Idx = getKspace(Kpomp,mplxRF1[0].FOV,mb_factor);

        /*loop over all k-space locations*/
        for(i=0;i<mb_factor;i++){
            /*fprintf(stderr,"Kpomp %f \n",Kpomp[i]);*/
            multiplexPulse_modulate(&mplxRF1[i],mb_factor,Kpomp[i],0.0);
            sprintf(mplxRF1[i].name,"mplxPulse_K%i",i);
        }
    }
    else{
        K0_Idx = 0; /*just to be safe*/
    }

    /*----------------------------------------------------------------*/
    /* add a single band calibration scan (if mb_factor > 1)        */
    /*----------------------------------------------------------------*/
    if(mb_single_calib_cv && (mb_factor>1)){

      /*init single band pulse was not done before */
      multiplexPulse_modulate(&mplxRF1[mb_factor],1,0.0,0.0);
      sprintf(mplxRF1[mb_factor].name,"mplxPulse_single%i",0);
      /*fprintf(stderr, "wave in prep = %i \n" ,mplxRF1[mb_factor].iwave_mag[0]); */
    }

    /*link pulse with user input and excitation gradients */
    mplxRF1[K0_Idx].alpha = opflip;
    mplxRF1[K0_Idx].a_RF = mplxRF1[K0_Idx].alpha/mplxRF1[K0_Idx].nom_alpha;

    multiplexPulse_link_rfpulse(&mplxRF1[K0_Idx],rfpulse,RF1_SLOT);

    /*and finally, prepare excitation sequence building block*/
    sbb_exc_cveval(&excSBB,&(mplxRF1[K0_Idx]));

    rfpulse[RF1_SLOT].num = 1;

    write_log("cveval after exc init\n");

    /*-------------------------------------------------------*/
    /*BZ prepare multiplex refocusing*/
    /*-------------------------------------------------------*/
    /*initialize single band wave*/
    multiplexPulse_initWaveFromFile(&mplxRF2[0],"rfse1b4.rho","none",RES_SE1B4_RF2);

    write_log("cveval after rfse1b4.rho init\n");

    multiplexPulse_initWaveParams(&mplxRF2[0],MAX_B1_SE1B4_180,NOM_BW_SE1B4,NOM_FA_SE1B4_RF2,NOM_PW_SE1B4_RF2,RES_SE1B4_RF2);

    mplxRF2[0].useWongPhases = optimSAR;
    mplxRF2[0].slcScaling = 1.0;

    multiplexPulse_stretchWave(&mplxRF2[0],stretchRF);

    /* slwid180 < 1 ==> slice select gradient gets reduced by that amount, results in wider slice */
    if (ampslice(&mplxRF2[0].slcSelectAmp,mplxRF2[0].bw,opslthick,slwid180, TYPDEF) == FAILURE) {return FAILURE;}

    /*invert gradient orientation for refocussing pulse = fat suppression ?*/
    if(first180_pol == -1)
    mplxRF2[0].slcSelectAmp = -mplxRF2[0].slcSelectAmp;

    /*make sure refocusing pulse has same geometry as excitation*/
    multiplex_sliceLocs(&mplxRF2[0],scan_info,opslquant,mb_factor);

    /*zero phase modulation for first rf pulse and 90deg phase difference between excitation and refocussing pulse*/
    multiplexPulse_modulate(&mplxRF2[0],mb_factor,0.0,PI/2.0);

    /*calculate SAR relevant parameters*/
    multiplexPulse_rfstat(&mplxRF2[0]);

    /*display pulse parameters*/
	if(debugstate)
    multiplexPulse_print(&mplxRF2[0]);

    /*export to text file */
    sprintf(mplxRF2[0].name,"mplx180_mb%i",mb_factor);
    /*multiplexPulse_export(&mplxRF2[0]);*/

    /*init all pomp pulses with properties from zero-phase pulse /*/
    multiplexPulse_initPompArray(&mplxRF2[0],mplxRF2,mb_factor+1);

    /*-------------------------------------------*/
    /*BZ pomp encoding (if mb_factor > 1)        */
    /*-------------------------------------------*/
    if(mb_rfmod_cv || pomp_calib_cv){
        /*loop over all k-space locations, same K as for excitation pulse*/
        for(i=0;i<mb_factor;i++){
            multiplexPulse_modulate(&mplxRF2[i],mb_factor,Kpomp[i],PI/2.0);
            sprintf(mplxRF2[i].name,"mplx180_K%i",i);
        /*multiplexPulse_export(&mplxRF2[n]);*/
        }
    }

    /*----------------------------------------------------------------*/
    /* add a single band calibration scan (if mb_factor > 1)        */
    /*----------------------------------------------------------------*/
    if(mb_single_calib_cv && (mb_factor>1)){
    
      /*init single band pulse was done before */
      multiplexPulse_modulate(&mplxRF2[mb_factor],1,0.0,0.0);
      sprintf(mplxRF1[mb_factor].name,"mplx180_K%i",0);
    
    }

    /*link pulse and excitation gradients */
    SBB180.stretchFactor = stretchRF;

    /*link sbb to rfpulse structure*/
    mplxRF2[K0_Idx].alpha = flipRF2;
    mplxRF2[K0_Idx].a_RF = mplxRF2[K0_Idx].alpha/mplxRF2[K0_Idx].nom_alpha;

    multiplexPulse_link_rfpulse(&mplxRF2[K0_Idx],rfpulse,RF2_SLOT);

    SBB180.prfpulse = &mplxRF2[K0_Idx];
    sbb_refoc_cveval(&SBB180);
    /*_________________________________________________________*/
    /*End of multi-band preparation*/

    write_log("cveval after multi-band preparation \n");
    
        /*-------------------------------------------------------*/
        /*Readout bandwidth etc.                      		 */
        /*-------------------------------------------------------*/
	calcvalidrbw((double)oprbw, &bandwidth, &max_rbw, &decimation, OVERWRITE_OPRBW, 0);
	tsp = TSP * decimation;
	fprintf(stderr,"bandwidth = %f [kHz] \n",bandwidth);
	fprintf(stderr,"tsp = %f [microsec] \n",tsp);
	fprintf(stderr,"decimation = %f  \n",decimation);
	fprintf(stderr,"GRAD_UPDATE_TIME = %i  \n",GRAD_UPDATE_TIME);

    
    
        /*only do these operations after timeout*/
        if( difftime(time(NULL), lasttime) > 1.0 )
        {
        /*-------------------------------------------------------*/
        /*MA Calculating readout waveforms                       */
        /*-------------------------------------------------------*/

#ifdef SIM
        fprintf(stderr,"\tassigning w structure\n");
        fflush(stderr);
#endif

	gslew_cv = (float)(10000.0 * (float)cfxfs / (float)cfrmp2xfs); /* cfsrmode; is wrong */

        init_ro(&w);
        w.fovx = opfov * 0.1;
        w.fovy = opfov * 0.1 * opphasefov;
        w.fovz = fovz_cv * 0.1;
        w.ni = ni_cv;
        w.nx = opxres;
        w.ny = opyres;
        w.nz = nz_cv;   
	w.dts =  GRAD_UPDATE_TIME * 1.0e-6;
	w.dts_adc =  tsp * 1.0e-6;
        w.gmax = gmax_cv; /*maximum gradient amplitude to be used by EPI */
        w.gslew = gslew_cv;
        w.Tmax = tsp * GRESMAX * 1.0e-6;
        w.ro_type = ro_type_cv;
        w.fn = fn_cv;
        w.fn2 = fn2_cv;
        w.epi_rampsamp = epi_rampsamp_cv;
        w.epi_blip_up_down = epi_blip_up_down_cv;
        w.rewind_readout = rewind_readout_cv;
        w.permute = permute_cv;

        /*BZ multi-band blips*/
        w.epi_multiband_factor = mb_factor;
        w.epi_multiband_fov = 10*mplxRF1[0].sliceSeparation*mb_factor; /*w.epi_multiband_d requires [mm]*/
        w.epi_multiband_R = mb_factor/mb_acceleration;

        /* rewind everything if the number of echoes is greater than 1 */
        if( opnecho >= 2 )
        {
            cvoverride(rewind_readout_cv, 1, 1, 1);
            cvoverride(opuser13, 1, 1, 1);
            w.rewind_readout = 1;
            w_nav.rewind_readout = 1;
        }

        /* navigator */
        init_ro(&w_nav);
        w_nav.nx = n_nav_x;
        w_nav.ny = n_nav_y;
        w_nav.nz = n_nav_x;
        w_nav.ro_type = nav_type_cv;
        w_nav.fovx = opfov * 0.1;
        w_nav.fovy = opfov * 0.1 * opphasefov;
        w_nav.fovz = fovz_cv * 0.1;
        w_nav.ni = ni_nav_cv;
        w_nav.dts = 4.0e-6;
        w_nav.gmax = gamp_cv;
        w_nav.gslew = gslew_cv;
        w_nav.Tmax = tsp * GRESMAX * 1.0e-6;
        w_nav.fn = fn_cv;
        w_nav.fn2 = fn2_cv;
        w_nav.epi_rampsamp = epi_rampsamp_cv;
        w_nav.rewind_readout = rewind_readout_cv;
        w_nav.reverse_readout = reverse_readout_cv;
        w_nav.permute = permute_cv;
        w_nav.R = R_nav_cv;
        w_nav.R_skip = R_skip_cv;
        /* no blip up & down for nav*/
        w_nav.epi_blip_up_down = epi_blip_up_down_cv;

        /* setting up the epi ref readout */
        if( epi_ref_cv )
        {
            /* epi refscan */
            init_ro(&w_epi_ref);

            w_epi_ref.nx = w.nx;
            w_epi_ref.ny = 6 * ni_cv; /* this is an arbitrary fixed number*/
            w_epi_ref.nz = 1;
            w_epi_ref.ro_type = RO_TYPE_EPI;
            w_epi_ref.fovx = opfov * 0.1;
            w_epi_ref.fovy = opfov * 0.1 * opphasefov;
            w_epi_ref.ni = ni_cv;
            w_epi_ref.dts = 4.0e-6;
            w_epi_ref.gmax = gamp_cv;
            w_epi_ref.gslew = gslew_cv;
            w_epi_ref.Tmax = tsp * GRESMAX * 1.0e-6;
            /*w_epi_ref.vdspiral_alpha = vdspiral_alpha_cv;*/
            w_epi_ref.fn = 1.0;
            w_epi_ref.fn2 = 1.0;
            w_epi_ref.epi_rampsamp = epi_rampsamp_cv;
            w_epi_ref.rewind_readout = 1; /* always rewind*/
            /*w_epi_ref.golden_angle = golden_angle_cv;*/
            w_epi_ref.reverse_readout = reverse_readout_cv;
            w_epi_ref.permute = permute_cv;
            /* turn off y and z axis */
  
            /*w_epi_ref.R = R_cv;*/
            w_epi_ref.R_skip = 1;
            w_epi_ref.epi_blip_up_down = 0;
	    
	    w_epi_ref.epi_multiband_factor = 1;
	    w_epi_ref.epi_multiband_fov = 10; /*w.epi_multiband_d requires [mm]*/
	    w_epi_ref.epi_multiband_R = 1;

            gen_base_ro(&w_epi_ref);

        }

        /*if( debugstate )
            write_log_3(&w);*/

        /*ADD_NEW_RO_TYPE*/
        gen_base_ro(&w);
        if( w_nav.ro_type != -1 )
        {
            gen_base_ro(&w_nav);
        }

        /* setting the number of imaging echoes */
        opnecho_imag = opnecho - (w_nav.ro_type > -1) - epi_ref_cv;

        lasttime = time(NULL);
    }

    /*-------------------------------------------------------*/
    /*Calculate timing for excitation and readout         */
    /*-------------------------------------------------------*/

    /* time from the start of the excitation pulse to the magnetic isocenter */
    RFiso2end = excSBB.time_from_RFcenter;

    
    /*dynamic echo time*/
    if(domap || (w.ni>1)){
	maxWait = 1200;
    }
    else{
	maxWait = 1000us;
    }

    /* 2014_04_09 murat, adding the effect of the epi refscan for minte calculation*/
    if( epi_ref_cv )
    {
        RFiso2end += GRAD_UPDATE_TIME * MAX(MAX(w_epi_ref.gx_res,w_epi_ref.gy_res),w_epi_ref.gz_res) + 8us;
    }

    /*time from start of readout to center*/
    time2echo = GRAD_UPDATE_TIME * w.gx_ctr + 8us;

    if( nav_pos == 1 || nav_pos == 3 || w_nav.ro_type == -1 )
    {
        time2echo = 4 * MAX(MAX(w.gx_ctr,w.gy_ctr),w.gz_ctr) + 8us;
    }
    else if( (nav_pos == 0 || nav_pos == 2) && w_nav.ro_type != -1 )
    {
        time2echo = 4 * MAX(MAX(w_nav.gx_ctr,w_nav.gy_ctr),w_nav.gz_ctr) + 8us;
    }


    /*-------------------------------------------------------*/
    /* prepare dpfg pulse                             */
    /*-------------------------------------------------------*/
	
    /*get two b-values from UI*/
    dpfgSBB.mode = dpfgMode_cv;
    dpfgSBB.antiparallel = antiparallel_cv;
    dpfgSBB.debug = 1;
	
    if(sbb_dw_dpfg_cveval(&dpfgSBB,&(mplxRF2[0]),bval1_cv,bval2_cv) == FAILURE){
	    epic_error(use_ermes, "Oops! Request longer mixing time. %.1f.\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 5.0);
        /*return FAILURE;*/
    }	
    
    /*calculate minTE (depends on dpfg-mode)*/
      switch(dpfgSBB.mode){

	case SSE: 
	    minte = MAX(2*(excSBB.time_from_RFcenter +2*dpfgSBB.m_dgrad.total_time+dpfgSBB.m_sbb180.total_time/2),2*(time2echo +2*dpfgSBB.m_dgrad2.total_time+dpfgSBB.m_sbb180.total_time/2));
	    break;
	case DSE:
	    minte = MAX(MAX(4*(excSBB.time_from_RFcenter + dpfgSBB.time_to_RF1),4*(time2echo + dpfgSBB.time_from_RF2)),2*dpfgSBB.time_RF1_to_RF2);
	    break;
	case HSE:
	    minte = MAX(MAX(4*(excSBB.time_from_RFcenter + dpfgSBB.time_to_RF1),4*(time2echo + dpfgSBB.time_from_RF2)),2*dpfgSBB.time_RF1_to_RF2);
	    break;
	case DTI:
	    minte = MAX(2*(excSBB.time_from_RFcenter + dpfgSBB.time_to_RF1),2*(time2echo + dpfgSBB.time_from_RF2));
	    break;

      }

      /*final TE calculation including additional echo time*/
      deltaTE_psd = RUP_GRD((int) deltaTE_cv*1000);

      sbb_dw_dpfg_updateTE(&dpfgSBB,minte + deltaTE_psd, deltaTE_psd);
      

      switch (dpfgSBB.mode){
	case SSE:
	  tefill1 = RUP_GRD((minte)/2) - excSBB.time_from_RFcenter - dpfgSBB.time_to_RF1;
	  tefill2 = RUP_GRD((minte)/2) - dpfgSBB.time_from_RF2 - time2echo;
	  break;
	case DSE:
	  tefill1 = RUP_GRD((minte + deltaTE_psd)/4) - excSBB.time_from_RFcenter - dpfgSBB.time_to_RF1; 
	  tefill2 = RUP_GRD((minte + deltaTE_psd)/4) - dpfgSBB.time_from_RF2 - time2echo;
	break;
	case DTI:
	  tefill1 = RUP_GRD((minte + deltaTE_psd)/2) - excSBB.time_from_RFcenter - dpfgSBB.time_to_RF1; 
	  tefill2 = RUP_GRD((minte + deltaTE_psd)/2) - dpfgSBB.time_from_RF2 - time2echo;
	break;
      }
      
      
      /*prepare spoiler gradient */
      sbb_spoil_grad_cveval(&spoilSBB,4);

      /*write to file*/
      if(debugstate){
	FILE *file;
	char fname[128];
	sprintf(fname,"%s","dPFG.host.log");
	file = fopen(fname,"w");

	  fprintf(file,"\t minte=%d \n",minte);
	  fprintf(file,"\t TE =%d \n",(int) (minte + deltaTE_psd));
	  fprintf(file,"\t RUP_GRD(minte/4)=%d \n",RUP_GRD(minte/4));
	  fprintf(file,"\t dpfgSBB.time_to_RF1=%d \n",dpfgSBB.time_to_RF1);
	  fprintf(file,"\t dpfgSBB.time_RF1_to_RF2=%d \n",dpfgSBB.time_RF1_to_RF2);
	  fprintf(file,"\t dpfgSBB.time_from_RF2=%d \n",dpfgSBB.time_from_RF2);
	  fprintf(file,"\t dpfgSBB.tmix=%d \n",dpfgSBB.tmix);
	  fprintf(file,"\t dpfgSBB.tmixMin=%d \n",dpfgSBB.tmixMin);
	  fprintf(file,"\t dpfgSBB.tfill1=%d \n",dpfgSBB.tfill1);
	  fprintf(file,"\t dpfgSBB.tfill2=%d \n",dpfgSBB.tfill2);
	  fprintf(file,"\t dpfgSBB.m_dgrad.total_time=%d \n",dpfgSBB.m_dgrad.total_time);
	  fprintf(file,"\t dpfgSBB.m_dgrad2.total_time=%d \n",dpfgSBB.m_dgrad2.total_time);
	  fprintf(file,"\t dpfgSBB.m_sbb180.total_time=%d \n",dpfgSBB.m_sbb180.total_time);
	  fprintf(file,"\t MAX1=%d \n",excSBB.time_from_RFcenter + dpfgSBB.time_to_RF1);
	  fprintf(file,"\t MAX2=%d \n",(int)(time2echo + dpfgSBB.time_from_RF2));
	  fprintf(file,"\t MAX3=%d \n",dpfgSBB.time_RF1_to_RF2);
	  fprintf(file,"\t time2echo=%f \n",time2echo);
	  fprintf(file,"\t tefill1=%d \n",tefill1);
	  fprintf(file,"\t tefill2=%d \n",tefill2);
	fclose(file);
      }
	
    /*-------------------------------------------------------*/
    /* count rf-pulses                             */
    /*-------------------------------------------------------*/
    /* whether or not to use 180 between readouts */
    for( i = 0; i < opnecho_imag - 1; i++ )
    {
        /* when you write 0 0 1 0 0 ..., this should visually match the 180's on the sequence.
         * That is why I am reversing */
        multiecho_180_bitmask_arr[opnecho_imag - i - 2] = ((unsigned int)multiecho_180_bitmask & ((unsigned int)1 << i))
            > 0;
    }

    rfpulse[RF1_SLOT].num = 1;
    /*gradz[GZRF1_SLOT].num = 1;*/

    rfpulse[RF2_SLOT].num = 0;

    if( exist(oppseq) == PSD_SE )
    {
		switch(dpfgSBB.mode){
			case SSE:
				rfpulse[RF2_SLOT].num = 1;
				break;
			case DSE:
				rfpulse[RF2_SLOT].num = 2;
				break;
			case HSE:
				rfpulse[RF2_SLOT].num = 3;
				break;
			case DTI:
				rfpulse[RF2_SLOT].num = 1;
				break;			  
			default:
			break;
		}
    }
    else
    {
	rfpulse[RF2_SLOT].activity = PSD_PULSE_OFF;
    }

    /* navigator */
    if( w_nav.ro_type != -1 && nav_pos >= 2 )
    {
        rfpulse[RF2_SLOT].num++;
    }

    /* multiecho imaging readout with 180 pulses NOT now */
    for( i = 0; i < opnecho_imag - 1; i++ )
    {
        if( multiecho_180_bitmask_arr[i] )
        {
            rfpulse[RF2_SLOT].num++;
        }
    }
@inline Prescan.e PScveval

    /* check that opte !< minte */
    cvmin(opte, minte);
    /*cvdef(opte, minte);*/
    if( (exist(opautote) == PSD_MINTE) || (exist(opautote) == PSD_MINTEFULL) ){
        cvunlock(opte);
        opte = minte + deltaTE_psd;
    }
    ihte1 = opte;

    /* the number of B0 and diffusion directions */
   if(opdiffuse == PSD_ON){
         num_tensor = exist(opdifnumdirs);
         num_B0     = exist(opdifnumt2);
         num_dif_vol= num_tensor + num_B0;
       }else{
         num_tensor = 0;
         num_B0     = 1;
         num_dif_vol= 1;      
       }


%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_cveval
%endif

    if( debugstate )
        write_log("\tend\n");
  	
    return SUCCESS;
}


/*DV 26 */
%ifdef DV26
optval min;
void getAPxParam(optval *min, optval *max, optdelta *delta, optfix *fix, float coverage, int algorithm)
{
  
}
int getAPxAlgorithm(optparam *optflag, int *algorithm){
  return APX_CORE_NONE;
}
%endif

int cvcheck()
{
    return SUCCESS;
}


int predownload()
{

    int pdi, pdj;
    int offset2;
    int tmpndd;
    int frsize;
    int trmin, seqtr;
    double b1rms_eval;

    /* image header variables set for correct annotation */
    ihflip = opflip;
    ihnex = opnex;
    ihtr = optr;

    /*  figure out some params depending on system/grads  */
    /*gamp_cv = loggrd.xfs;*/
    /*gslew_cv = (float)(10000.0 * (float)cfxfs / (float)cfrmp2xfs);*/ /* cfsrmode; is wrong */


    /*BZ check if multiband acceleration can be fullfilled*/
    if((opslquant%mb_factor != 0) && (mb_factor > 1))
    {
        epic_error(use_ermes, "Oops! Number of slices divided by multiband factor must be an integer.  %.1f .\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 0.0);
        return FAILURE;
    }

   /*BZ check if in-plane acceleration comes with appropriate number of interleaves*/
    if(R_cv >  ni_cv)
    {
        epic_error(use_ermes, "Oops! Number of interleaves and in-plane acceleration Ry don't match. %.1f.\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 0.0);
        return FAILURE;
    }
    
    
    /*BZ no support for multi-shot epi for DWI*/
    if(R_cv < ni_cv){
       epic_error(use_ermes, "Oops! %d interleaves prescribed without in-plane acceleration. Multi-shot DWI not supported. \n", EM_PSD_SUPPORT_FAILURE, 1, INT_ARG,(int) ni_cv);
        return FAILURE;
    }
    

   /*BZ we only allow even number of slices */
    if(((opslquant/mb_factor)%2 !=0) && (opslquant > 1))
    {
        epic_error(use_ermes, "Oops! Number of effective slices (Nslices/mb_factor) must be even.  %.1f .\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 0.0);
        return FAILURE;
    }

    /*BZ check if waveform is too long*/
    if(w.gx_res > 22200){
       epic_error(use_ermes, "Oops! Readout waveform too long.  %d .\n", EM_PSD_SUPPORT_FAILURE, 1, INT_ARG,w.gx_res);
        return FAILURE;
    }

   /*BZ for now disable SSE and dTE > 0 */
    if((dpfgMode_cv == SSE) && (deltaTE_cv > 0))
    {
        epic_error(use_ermes, "Oops! SSE mode and (dTE > 0) not yet supported.  %.1f .\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 0.0);
        return FAILURE;
    }
    
    /*BZ tensor.dat only for DTI */
    if((dpfgMode_cv != DTI) && (fileId_cv == -1))
    {
        epic_error(use_ermes, "Oops! tensor.dat can only be used for DTI.  %.1f .\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, 0.0);
        return FAILURE;
    }


    /* make TR check */
    /* this part is a decimated copy of the related portion in pulsegen */
    /* make sure opte is right at this point */

    /* start of the sequence */
    pos_start = tlead + PSD_PRERFAMP_UBL + 14;

    /* beggining of 1st rf pulse */
    pos_start_rf = pos_start  /*+ blip_time*/;
    if (opfat==1)
        pos_start_rf+=FatSatSBB.total_time;

    RFiso2end = RUP_GRD(pos_start_rf + excSBB.time_to_RFcenter);
    
    if (opfat==1)
        RFiso2end+=FatSatSBB.total_time;


    offset2 = pos_start_rf;/*already includes potential fat-sat pulse*/
    offset2 += excSBB.total_time;
  
    /* start generating the WF_PULSE structures */
    offset2 += tefill1;
    offset2 += dpfgSBB.total_time;
    offset2 += tefill2;
    offset2 += (w.gx_res*GRAD_UPDATE_TIME + 8us);

    /* adding the spoiler and the dead time */   
    offset2 += (spoilSBB.total_time + timessi); 
    offset2 += 2000; /*add a little safety margin*/

    trmin = offset2;

    seqtr = trmin * (opslquant/mb_factor);

    /*fprintf(stderr,"seqtr, trmin, psdseqtime=  %d  %d  %d\n", seqtr,trmin,psdseqtime);*/
    if( seqtr > optr )
    {
        epic_error(use_ermes, "Oops! optr must be > %.1f ms.\n", EM_PSD_SUPPORT_FAILURE, 1, FLOAT_ARG, (seqtr+500) / 1000.0);
        return FAILURE;
    }

    psdseqtime = RUP_GRD(optr/(opslquant/mb_factor));


%ifdef RTMOCO
    gmax_factor=0.9;
%endif

    gamp_cv = gamp_cv * gmax_factor;
    gslew_cv = gslew_cv * gmax_factor;

    if( gamp_cv > loggrd.xfs )
    {
        epic_error(0, "gamp (=%f) too large! please decrease gamp or bandwidth. max gamp = %f\n", 0, 2, FLOAT_ARG,
                   gmax_cv, FLOAT_ARG, gamp_cv);
        return FAILURE;
    }

#include "predownload.in"


    /*How many frames, interleaves, etc... */
    if( opdiffuse || optensor )
    {
        num_dif_vol = opdifnumdirs;
        nframes = num_dif_vol + num_B0; /* was in diff_fun.e rafael pulled it out and stuck it here */ 
    }

    /*BZ if we do pomp calibration we acquire more time frames*/
    if(pomp_calib_cv){
        nframes = nframes + mb_factor;
    }

    /*BZ if we do single band calibration we acquire more time frames*/
    if(mb_single_calib_cv){
        nframes = nframes + 2*mb_factor;
    }
    

    if( pass_multiple_pfiles == 0 )
        nframes_per_pfile = nframes;
    else
        nframes_per_pfile = 1;

    
    /*how many recorded interleaves*/
    data_interleaves = (ni_cv/R_cv) + epi_blip_up_down_cv;

    tmpndd = (nextra + opnex - 1) / opnex;
    
    /*how many TRs (=calibration + dummy scans + interleaves + nframes)*/
    nbang = (nextra + data_interleaves * nframes / opnecho_imag) * opslquant;
    nbang = (2*mb_factor  /*blip up & down single band calibration scans*/
	    + nextra /*dummy scans without interleaves */
	    + (data_interleaves/opnecho_imag)*(num_dif_vol + num_B0))* opslquant;

   
    /* set up clock */
    if( (exist(opcgate) == PSD_ON) && existcv(opcgate) )
    {
        pidmode = PSD_CLOCK_CARDIAC;
        piviews = tmpndd + w.ni * nframes / opnecho_imag;
        piclckcnt = ophrep;
        pitscan = (float)(optr) * nbang / opslquant;
        pitslice = psdseqtime;
    }
    else
    {
        pidmode = PSD_CLOCK_NORM;
        pitslice = psdseqtime;
        pitscan = (float)(optr) * nbang / opslquant;
    }


@inline loadrheader.e rheaderinit

    rhimsize = 64;
    while( rhimsize < opxres )
        rhimsize *= 2;
    if( off_fov == 1 )
    {
        rhxoff = 0;
        rhyoff = 0;
    }
    else
    {
        /*rhxoff = rhimsize * scan_info[0].oprloc / opfov;
        rhyoff = rhimsize * scan_info[0].opphasoff / opfov;*/
	rhxoff = scan_info[0].oprloc;
	rhyoff = scan_info[0].opphasoff;
    }

    /* fill in rhuser variables with the w structure*/
    rhuser0 = w.ro_type;
    /*!!!! */
    rhuser1 = w.fovx;
    rhuser2 = ni_cv;
    rhuser3 = w.nx;
    rhuser4 = w.ny;
    rhuser5 = w.nz;
    rhuser6 = w.dts;
    rhuser7 = w.gmax;
    rhuser8 = w.gslew;
    rhuser9 = w.dts_adc;
    rhuser10 = n_nav_x;
    rhuser11 = deltaTE_cv;
    rhuser12 = nframes;
    rhuser13 = oppseq;
    rhuser14 = w.fovz;
    rhuser15 = opslice_order;
    rhuser16 = bval1_cv;
    rhuser17 = exist(opdifnumdirs);
    rhuser18 = exist(opdifnumt2);
    rhuser19 = exist(opbval);
    rhuser20 = 20170201;
    rhuser21 = w.epi_rampsamp;
    rhuser22 = dpfgMode_cv;
    rhuser23 = w.rewind_readout;
    rhuser24 = nav_type_cv;
    rhuser25 = bval2_cv;
    rhuser26 = w.reverse_readout;
    rhuser27 = nav_pos;
    rhuser28 = antiparallel_cv;
    rhuser29 = w.epi_multiband_factor;
    rhuser30 = w.epi_multiband_fov;
    rhuser31 = epi_ref_cv;
    rhuser32 = w.epi_multiband_R;
    rhuser33 = n_nav_y;
    rhuser34 = w.fn;
    rhuser35 = w_nav.ni;
    rhuser36 = w.permute;
    rhuser37 = w.fn2;
    rhuser38 = w.R;
    rhuser39 = w_nav.R;
    rhuser40 = w.R_skip;
    rhuser41 = multiecho_180_bitmask;
    rhuser42 = w.epi_blip_up_down;
    /*rhuser43 to 48 are not used so far*/
    /*store sliceloc[0]*/
    rhuser43 = scan_info[0].optloc;
    rhuser44 = epi_blip_up_down_cv;
    rhuser45 = mb_single_calib_blip_down_cv;
    rhuser46 = fileId_cv;
    rhuser47 = mb_single_calib_cv;
    if(mb_single_calib_blip_down_cv)
      rhuser47 = rhuser47 + 1;
    rhuser48 = R_cv;
    /*ADD_NEW_RO_TYPE*/

    rhbline = 0;

    rhb0map = domap; /*fieldmap on/off*/
    rhtediff = 1000; 

    rhnslices = opslquant;
    /*rhnrefslices = opslquant;*/
    rhrcctrl = 1; /* lx wants to create images.  */
    rhexecctrl = 2; /* just save the raw data */
    /*rhdacqctrl = 0; */
    rhrecon = 930; /* invoke son of recon */

    autolock = 1; /* save p-files */
    /*rhnphases = nframes;*/

    /*acqs = 1;*/
    /*  slquant1 = rhnslices;*//* murat */
    slquant1 = opslquant/mb_factor;

    /* for straight sequential order of slices. */
    if( !orderslice(slice_order, opslquant, opslquant, gating) )
        epic_error(use_ermes, "orderslice call failed", 0, 0);

    /* initialize copy of original rotation matrices *//*BZ changed form rhnslices to opslquant*/
    for( pdi = 0; pdi < opslquant; pdi++ )
    {
        for( pdj = 0; pdj < 9; pdj++ )
        {
            rsprot[pdi][pdj] = hostToRspRotMat(scan_info[pdi].oprot[pdj]);
        }
    }
    scalerotmats(rsprot, &loggrd, &phygrd, opslquant, 0);/*BZ changed from rhnslice to opslquant*/

    /* save stuff for maxwell correction */
    rhmaxcoef1a = rsprot[0][0] / (float)cfxfull; /* save x rotator */
    rhmaxcoef1b = rsprot[0][1] / (float)cfxfull;
    rhmaxcoef2a = rsprot[0][3] / (float)cfyfull; /* y  */
    rhmaxcoef2b = rsprot[0][4] / (float)cfyfull;
    rhmaxcoef3a = rsprot[0][6] / (float)cfzfull; /* z  */
    rhmaxcoef3b = rsprot[0][7] / (float)cfzfull;

    rhdab0s = cfrecvst;
    rhdab0e = cfrecvend;

    if( entrytabinit(entry_point_table, (int)ENTRY_POINT_MAX) == FAILURE)
    {
        epic_error(use_ermes, "Can't initialize entry point table.", 0, 0);
        return FAILURE;
    }


    /* set up receiver */
    initfilter();

    cvmax(rhfrsize, 32768); /* for now  */

    /* sanity check for the number of echoes */

    /* even when the imaging readout has mutiple echoes, I load them as
     * different views into hard drive. So, in order for the read_pfile not to get confused
     * I should set the rhnecho header variable to either 1 or 2
     */
    /*rhnecho=opnecho;*/

    if( w_nav.ro_type == -1 )
    {
        frsize = w.frsize*(((float)GRAD_UPDATE_TIME)/tsp);
        rhnecho = 1;
    }
    else
    {
        frsize = MAX(w.frsize,w_nav.frsize);
        rhnecho = 2;
    }
    if( epi_ref_cv )
        rhnecho = 3;

    if( debugstate )
    {
        fprintf(stderr, "\tfrsize = %d opnecho = %d w_nav.frsize %d  w_imag.frsize %d\n", frsize, opnecho, w_nav.frsize,
                w.frsize);
        fflush(stderr);
    }
    
    /*BZ check if number of points in readout is too large. The limit seems to be 16384*/
    if(frsize > 16000){
       epic_error(use_ermes, "Oops! Number of points in ADC too large (frsize = %d > 16384). Lower bandwidth or shorten readout.  %d .\n", EM_PSD_SUPPORT_FAILURE, 1, INT_ARG,frsize);
        return FAILURE;
    }

    rhfrsize = frsize; /* num points sampled */
    total_views = 2 * ((ni_cv * nframes_per_pfile + 1) / 2); /* has to be an even number */
    total_views = 2 * ((data_interleaves * nframes_per_pfile + 1) / 2); /* has to be an even number */
    cvmax(rhnframes, total_views);

    rhnframes = total_views;

    if( debugstate )
        write_log("predownload - calcfilter\n");

    if( calcfilter(&echo_filt, bandwidth, rhfrsize, OVERWRITE_OPRBW) == FAILURE)
    {
        epic_error(use_ermes, "%s failed", EM_PSD_SUPPORT_FAILURE, 1, STRING_ARG, "calcfilter");
        return FAILURE;
    }
    setfilter(&echo_filt, SCAN);
    filter_echo = echo_filt.fslot;

    if( w_nav.ro_type != -1 )
    {
        if( calcfilter(&echo_filt_nav, bandwidth, w_nav.frsize * 4us / tsp, OVERWRITE_OPRBW) == FAILURE)
        {
            epic_error(use_ermes, "%s failed", EM_PSD_SUPPORT_FAILURE, 1, STRING_ARG, "calcfilter");
            return FAILURE;
        }
        setfilter(&echo_filt_nav, SCAN);
        filter_echo_nav = echo_filt_nav.fslot;
    }

    if( epi_ref_cv )
    {
        if( calcfilter(&echo_filt_epi_ref, bandwidth, w_epi_ref.frsize * 4us / tsp, OVERWRITE_OPRBW) == FAILURE)
        {
            epic_error(use_ermes, "%s failed", EM_PSD_SUPPORT_FAILURE, 1, STRING_ARG, "calcfilter");
            return FAILURE;
        }
        setfilter(&echo_filt_epi_ref, SCAN);
        filter_echo_epi_ref = echo_filt_epi_ref.fslot;
    }

    numrecv = rhdab0e - rhdab0s + 1;
    /*rhrawsize = 2*rhptsize*rhfrsize*(rhnframes+1)*rhnslices*rhnecho;*/

    /* I will load different views of the imaging readout into different view sections so echoes
     will be maximum two */

    rhnslices = opslquant/mb_factor; /*BZ can we overwrite it here  ?*/

    rhrawsize = 2 * rhptsize * rhfrsize * (rhnframes + 1) * rhnslices * rhnecho;


    daqdel = psd_grd_wait + daqdeloff + 0.5;

	/* for multi pfile */
	if (pass_multiple_pfiles==1){
		rhformat |= RHF_SINGLE_PHASE_INFO; 			
		rhnpasses =  num_dif_vol;
		rhnphases =  num_dif_vol;
		opfphases =  num_dif_vol;
		acqs      =  num_dif_vol;
		rhnslices =  opslquant*num_dif_vol;
		/*rhrawsize*=  num_dif_vol;*/
	} else {
		rhnpasses =  1;
		rhnphases =  1; 
		opfphases =  1; 
		acqs      =  1; 
	}

    /*  Adjust acgd_tr for the total number of images to be acquired.
     Apparently allocation of something takes a lot of time and can
     generate EOS errors in the acgd_transition mode routine- I don't
     get it but this works.  */

    nimages = (rhnframes + 1) * opslquant; /* don't need more for 2 echoes */
    acgd_tr = 50ms + RUP_GRD(0.036ms*nimages) * 2.0; /*MURAT*/

    strcpy(entry_point_table[L_SCAN].epname, "scan");
    entry_point_table[L_SCAN].epfastrec = 0;
    entry_point_table[L_SCAN].epstartrec = rhdab0s;
    entry_point_table[L_SCAN].ependrec = rhdab0e;
    entry_point_table[L_SCAN].epfilter = (unsigned char)echo_filt.fslot;
    entry_point_table[L_SCAN].epprexres = rhfrsize;


    entry_point_table[L_SCAN].epxmtadd = getCoilAtten();

    entry_point_table[L_APS2] = entry_point_table[L_MPS2] = entry_point_table[L_SCAN]; /* copy scan into APS2 & MPS2 */
    strcpy(entry_point_table[L_APS2].epname, "aps2");
    strcpy(entry_point_table[L_MPS2].epname, "mps2");
    entry_point_table[L_APS2].epprexres = psfrsize;
    entry_point_table[L_MPS2].epprexres = psfrsize;
    entry_point_table[L_APS2].epfilter = (unsigned char)filter_echo1mps1;
    entry_point_table[L_MPS2].epfilter = (unsigned char)filter_echo1mps1;

    trigfreq = opslquant; /* default for TTL trig is every TR */

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_predownload_2
%endif


    /* ************************************************
     RF Scaling
     Scale Pulses to the peak B1 in whole seq.
     ********************************************** */

    /* RF Pulse Scaling (to peak B1)  ************************/
    /* First, find the peak B1 for the whole sequence. */
    if (findMaxB1Seq(&maxB1Seq, maxB1, MAX_ENTRY_POINTS, rfpulse, RF_FREE) == FAILURE)
    {
        epic_error(use_ermes,supfailfmt,EM_PSD_SUPPORT_FAILURE,EE_ARGS(1),STRING_ARG,"findMaxB1Seq");
        return FAILURE;
    }
    /* Throw in an extra scale factor to account for xmtadd. */
    if( setScale(L_SCAN, RF_FREE, rfpulse, maxB1[L_SCAN], maxB1[L_SCAN] / maxB1Seq) == FAILURE)
    {
        epic_error(use_ermes, supfailfmt, EM_PSD_SUPPORT_FAILURE, EE_ARGS(1), STRING_ARG, "setScale");
        fprintf(stderr, "maxB1Seq = %f\n", maxB1Seq);
        fflush(stderr);
        return FAILURE;
    }

    ia_rf1 = max_pg_iamp * (*rfpulse[RF1_SLOT].amp);
    ia_rf2 = max_pg_iamp * (*rfpulse[RF2_SLOT].amp);

    /* BZ update pulse is handled within the sbb */

    ia_rf1 = MAX_PG_IAMP*mplxRF1[0].a_RF;
    ia_rf2 = mplxRF2[0].a_RF*MAX_PG_IAMP;

    eepf=0; oepf=0; eeff=0; oeff=0;
    set_echo_flip(&rhdacqctrl,&chksum_rhdacqctrl,0,0,0,0);
    
    /* ************************************************
    SAR calculation
     ********************************************** */
    /*if(peakAveSars(&ave_sar_eval,&cave_sar_eval,&peak_sar_eval,&b1rms_eval,(int)RF_FREE,rfpulse,L_SCAN,(int)(act_tr/slquant1))==FAILURE)
    {
	epic_error(use_ermes,supfailfmt, EM_PSD_SUPPORT_FAILURE,EE_ARGS(1),STRING_ARG,"peakAveSars");
    }*/

    /*Some prescan stuff  */
    pislquant = opslquant/mb_factor;

    /*wrt*/
    sprintf(seq_header_name,"%s",_header_name);

@inline Prescan.e PSfilter
@inline Prescan.e PSpredownload


%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_predownload
%endif

    if( debugstate )
    {
          logmessage( logfile, "predownload done", 0, 1); /*1= to file*/
    }

    rhtype1 |= RHTYP1BAM0FILL; /* set to zero-fill BAM before start of acquisition */
 

    return SUCCESS;
} /* End-Of-Predownload */


@inline Prescan.e PShost

@rsp
#include <stdlib.h> /*RTB0 correction*/
#include <stdio.h> /*RTB0 correction*/
#include <string.h> /*RTB0 correction*/

int acgd_control( void );

int pre = 3; /* prescan flag */

short thamp;
CHAR *entry_name_list[ENTRY_POINT_MAX] =
{   "scan", "mps2", "aps2",

@inline Prescan.e PSeplist

};


int* rf1_freq;
int* rf2_freq;
int* rf_freq_store;
int* rfnav_freq;
int* rfimag_freq;
/*int* RBfxmit3;*/
int* freq;
int* theta_freq;
int* rfphastab;
int* slcReorderIndex;
int* slcReorderIndexMB;
int *tf;        /* time factor shift */

float TENSOR_AGP[2*3][MAX_DTI_DIRECTIONS + MAX_T2];
/*#include "../bz_common/generic_readouts/generic_ro.c"
@inline ../bz_common/generic_readouts/generic_ro.e _rsp*/


@rspvar

int iv,iv_ro, ifr, sliceindex, iex, viewang, bufi, iv_ps;
/*BZ more rsp variables */
int mb_counter;
int pomp_calib_counter;
int singleSlice_counter;
int singleSlice_flag;
int ifr_start;
int tmp;
int i, k;
int bangn;
int trig, dtype, dabop;
int slcorder[200];
int diff_counter; /*might be different from time frame counter */
int scan_counter; 

short trigonpkt[3] = { 0, SSPOC + DREG, SSPD + DSSPD4 };
short trigoffpkt[3] = { 0, SSPOC + DREG, SSPD };
short trigonwd, trigoffwd;

/* variables needed for prescan */
short chopamp;
int seqCount, ec, rf1Phase, seqCount;
int rspent, rspdda, rspbas, rspvus, rspgy1, rspasl;
int rspesl, rspchp, rspnex, rspslq, rspsct;
int dabmask;
int view; /*DV26*/
int excitation; /*DV26*/
int debug; /*DV26 */
@inline Prescan.e PSrspvar /* For Prescan */
extern PSD_EXIT_ARG psdexitarg;
/*@inline ChemSat.e ChemSatRspVar */

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rspvar
%endif


@pg

#include <epic_loadcvs.h>
/* MRIge55206 - change memory allocation if in SIM for multi-dim array support */
#ifdef SIM
#include <stdlib.h>
#define AllocMem malloc
#else
#define AllocMem AllocNode
#endif
#include <math.h>
long deadtime_core;
/* long deadtime_trajcore; /\* trajectory measurement *\/ */

/* keep these declarations here otherwise I get an out of memory error trying to download to scanner */
generic_ro_psd w_psd[MAX_PSD_ECHOES];
generic_ro_psd w_psd_nav;
generic_ro_psd w_psd_epi_ref;

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_pg_fundef
%endif
#define myTGT
#include "../bz_common/mplx_rfpulse.c"
@inline ../bz_common/sbb_exc.e _pg
@inline ../bz_common/sbb_180.e _pg
@inline ../bz_common/sbb_nonSelective.e _pg
@inline ../bz_common/sbb_diff_grad.e _pg
@inline ../bz_common/sbb_spoil_grad.e _pg
@inline sbb_dw_dpfg.e _pg

#include "../bz_common/generic_readouts/generic_ro.c"
@inline ../bz_common/generic_readouts/generic_ro.e _rsp

int cs_satindex; /* index for multiple calls to chemsat routines */

STATUS pulsegen( void )
{
    int waitloc;
    int offset2; /* variable for keeping track of current pulse position */
    char txt[100];

    int j, i, k;

    if(debugstate > 1)
    {
	logmessage( logfile, "start pulsegen", 0, 1); /*1= to file*/ 
    }

    fprintf(stderr,"rhfrsize =%i \n",rhfrsize);

%ifdef RTMOCO
#ifdef IPG
    rtMoCo_init();
#endif
%endif


    /*  psd_board_type=PSDCERD; */
    sspinit(psd_board_type);


    int start_time_epi_ref;

    /*------------------------------------------------------------------------------*/
    /* Fat Sat Pulse                                                                */
    /*------------------------------------------------------------------------------*/
    if(opfat==1){
        FatSatSBB.prfpulse = &fatSatRF;
        FatSatSBB.start_time = pos_start_rf - FatSatSBB.total_time;
        sbb_nonSelective_pulsegen(&FatSatSBB,"FatSatSBB");
    }

    /*------------------------------------------------------------------------------*/
    /* BZ multi-band excitation building block                                      */
    /*------------------------------------------------------------------------------*/
        /*reassign pointers */
        /*multiplexPulse_print(&mplxRF1[0]);*/
        sbb_exc_pulsegen(&excSBB,"SBBexc",&mplxRF1[0],pos_start_rf);
        excSBB.Nwaves =1;

        start_time_epi_ref= pos_start_rf + excSBB.total_time ;
	
        /*assign RF-pulse pointers even if we are not using pomp. We need a single band pulse in any case */
        /*pulse waves are initialized with 1st wave*/
        for(i=0;i<=mb_factor;i++){
            excSBB.prfpulse_pomp[i] =  &mplxRF1[i];
        }
        excSBB.Nwaves = mb_factor+1;
	
        /*finally build wave forms for rest of rf-pulses but do NOT create instructions */
        sbb_exc_pulsegen_pomp(&excSBB);
 

    /*------------------------------------------------------------------------------*/
    /* putting in the epi_ref                                                       */
    /*------------------------------------------------------------------------------*/
    if( epi_ref_cv )
    {
        w_psd_epi_ref.filter_num = filter_echo_epi_ref;
        gen_ro_psd_2(&w_epi_ref, &(w_psd_epi_ref), "epi_ref", start_time_epi_ref, filter_echo_nav);
    }
    


    /* ---------------------------------------------------------------------------------- */
    /* GE (basically do nothing)  */
    /* ---------------------------------------------------------------------------------- */
    if( oppseq == 2 /*gradient echo*/ )
    {
        waitloc = RUP_GRD(RFiso2end);
    }


    
    /* ---------------------------------------------------------------------------------- */
    /* double diffusion preparation */
     /* ---------------------------------------------------------------------------------- */
    dpfgSBB.debug = 0;
    offset2 = excSBB.start_time + excSBB.total_time +  tefill1;
    dpfgSBB.start_time = offset2;
    sbb_dw_dpfg_pulsegen(&dpfgSBB,"DPFG",mplxRF2,mb_factor+1);
    if(debugstate)
      sbb_dw_dpfg_writeLog(&dpfgSBB,"dPFG.pg.log");
    offset2 = dpfgSBB.start_time + dpfgSBB.total_time;
    offset2 += tefill2;


    /* ---------------------------------------------------------------------------------- */
    /* no wait time  */
    /* ---------------------------------------------------------------------------------- */

	
    /* ---------------------------------------------------------------------------------- */
    /* EPI readout 									*/
    /* ---------------------------------------------------------------------------------- */
#ifdef IPG

    for (j=0;j<opnecho_imag;j++)
    {
        w_psd[j].filter_num=filter_echo;
    }
    if (w_nav.ro_type !=-1)
    {
        w_psd_nav.filter_num=filter_echo_nav;
    }
	
    /* start generating the WF_PULSE structures */	
	k=0;
	for (i=0;i<opnecho_imag;i++)
	{
		/*create the readout */
		sprintf(txt,"imag%d",i);
		gen_ro_psd_2(&w,&(w_psd[i]),txt,offset2,filter_echo);
		offset2+=w.gx_res*4+espace; /*end of ro */

			/* create the 180 if necessary */
			if (multiecho_180_bitmask_arr[i] && i!=opnecho_imag  -1)
			{
				/*SBB180.start_time = offset2;*/
				sbb_refoc_pulsegen(&SBB180,"SBB180",mplxRF2,offset2);
				offset2 +=SBB180.total_time ;

			}
	}


#endif


    if( debugstate )
    {
        fprintf(stderr, "\tnav filter : %d imag filter %d ni: %d\n", w_psd_nav.filter_num, w_psd[0].filter_num, w.ni);
        fprintf(stderr, "pulsegen >> generated waveforms\n");
        fflush(stderr);
    }
    
    
    sbb_spoil_grad_pulsegen(&spoilSBB,"spoilSBB",offset2);
    offset2 += spoilSBB.total_time;
	

    /* Pulse Pack for multi Pfiles */
    pos_pass_pulse = pbeg(&(spoilSBB.gz),"spoilSBB_gz",0);
/*@inline diff_fun.e diff_pass_pack*/
/************************************************************************/
/*		Passpack to do multiple pfiles				*/
/*									*/ 
/*		be sure to define pos_pass_pulse			*/
/************************************************************************/
/* pos_pass_pulse = pos_killer; */
PASSPACK(internal_pass_pulse, pos_pass_pulse);  	
/*--------------------------------------------------------------------*/

    /*  make a little trig on CAP J10  */
    if( maketrig )
    {
        trigonwd = SSPD + DABOUT6;
        trigoffwd = SSPD;
        if( zone_cntl == 1 )
        { /* preserve acgd bit */
            trigonwd += DABOUT3;
            trigoffwd += DABOUT3;
        }
        trigonpkt[0] = SSPDS + EDC;
        trigoffpkt[0] = SSPDS + EDC;
        trigonpkt[2] = trigonwd;
        trigoffpkt[2] = trigoffwd;
        SSPPACKET(trigon, trigloc + waitloc, 3us, trigonpkt, 0);
        SSPPACKET(trigoff, trigloc + waitloc + triglen, 3us, trigoffpkt, 0);
    }

%ifdef RTMOCO
#ifdef IPG 
@inline rtMoCo.e rtMoCo_pg_pulsegen_generic
#endif
%endif

    SEQLENGTH(seqcore, psdseqtime - timessi, seqcore);
    getperiod(&deadtime_core, &seqcore, 0);

    /*  pass packet sequence (pass).  */
    PASSPACK(endpass, 49ms);
    SEQLENGTH(pass, 50ms, pass);

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_pg_pulsegen
%endif

@inline Prescan.e PSpulsegen /*  prescan sequences  */

    /* control bit for ACGD */
    if( zone_cntl == 1 )
    { /* select exciter and set dabout 3 */
        short bm_acgdDABcontrol_ON[3] = { SSPDS, SSPOC + DREG, SSPD };
        bm_acgdDABcontrol_ON[0] = SSPDS + EDC;
        bm_acgdDABcontrol_ON[2] = SSPD + DABOUT3;
        SSPPACKET(DABcontrol_ON, 500, 3, bm_acgdDABcontrol_ON, 0);
        SEQLENGTH(seqON, acgd_tr, seqON);
    }

    /* pause entry point for giving sequence time at the beginning to load the first waveform*/
    /* this is to fix the psd pulse programming error an ssp eos was receive  message */
    SEQLENGTH(seq_pause, 500000, seq_pause);

    buildinstr(); /* load the sequencer memory */


    if( debugstate )
    {
        fprintf(stderr, "exiting pulsegen\n");
        fprintf(stderr,"------generic pulsegen info-------\n");
        fprintf(stderr,"excSBB.ia_rfExt=%i \n",excSBB.ia_rfExt);
        fprintf(stderr,"SBB180.ia_rfExt=%i \n",SBB180.ia_rfExt);
        fprintf(stderr,"opte %i \n",opte);
        fprintf(stderr,"minte %i \n",minte);
        fprintf(stderr,"satoff= %f \n",satoff);
        fprintf(stderr,"FatSatSBB.ia_rfExt=%i \n",FatSatSBB.ia_rfExt);
	fflush(stderr);
        
    }

    return SUCCESS;

}/* end of pulsegen */


/*@inline ChemSat.e ChemSatPG*/
@inline Prescan.e PSipg
/* end of @pg */

@rsp

STATUS scancore( void );
/* STATUS scancore_traj(void); /\* rafael the trajectory measurement *\/ */
void doleaf();
/* void doleaf_traj(); /\* rafael the trajectory measurement *\/ */

void sliceReordering(int* ,int ,int );

@inline Prescan.e PScore

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rsp_fundef
%endif

@inline ../bz_common/sbb_exc.e _rsp
@inline ../bz_common/sbb_180.e _rsp
@inline ../bz_common/sbb_nonSelective.e _rsp
@inline ../bz_common/sbb_diff_grad.e _rsp
@inline sbb_dw_dpfg.e _rsp

int mps2()
{

    pre = 2;
    acgd_control();
    scancore();
    rspexit();
    return SUCCESS;
}

int aps2()
{
    pre = 1;
    rspent = L_APS2;
    /*rspdda = ps2_dda;*/
    acgd_control();
    scancore();
    rspexit();
    return SUCCESS;
}

int scan()
{
    rspent = L_SCAN; /* hack 2014_04_16 */

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rsp_scan_init
@inline rtMoCo.e rtMoCo_rsp_scan_init_generic
%endif

    pre = 0;

    scancore();

    rspexit();

    return SUCCESS;
}

int acgd_control( void )
{
    if( zone_cntl == 1 )
    { /* run the acgd setup */
        boffset (off_seqON);
        settrigger(TRIG_INTERN, 0);
        startseq(0, MAY_PAUSE);
    }
    return SUCCESS;
}


STATUS scancore()
{

    int i;

#ifdef SIM
    fprintf(stderr,"entering scancore pre = %d\n",pre);
    fflush(stderr);
#endif

    if( maketrig == 1 )
    {
        setwamp(trigoffwd, &trigon, 2); /* no trigs yet */
    }
    /*  setrfconfig(ENBL_RHO1 + ENBL_THETA + ENBL_OMEGA);*/
    setrfconfig((short)141);
    setssitime((LONG)timessi / GRAD_UPDATE_TIME);
    rspqueueinit(queue_size);
    scopeon (&seqcore);
    syncon(&seqcore);
    syncoff (&pass);
    setrotatearray((SHORT)opslquant, rsprot[0]);
    for(i=0;i<opslquant;i++){
        /*settrigger(TRIG_INTERN, i);*/
        rsptrigger[i] = TRIG_INTERN;
    }
    settriggerarray((SHORT)opslquant, rsptrigger); /*BZ what is this doing?*/


    dabmask = PSD_LOAD_DAB_ALL;

    for( i = 0; i < opnecho_imag; i++ )
    {
        setrfltrs(filter_echo, &(w_psd[i].echo));
    }
    if( w_nav.ro_type != -1 )
    {
        setrfltrs(filter_echo_nav, &(w_psd_nav.echo));
    }
    if( epi_ref_cv )
    {
        setrfltrs(filter_echo_epi_ref, &(w_psd_epi_ref.echo));
    }

    /* fix the clock for aux trig */
    if( !pre && gating == TRIG_AUX )
    {
        setscantimemanual();
        setscantimestop();
        setscantimeimm(pidmode, pitscan, piviews, pitslice, opslicecnt);
    }

    /*BZ overwrite tensor CVs and DTI matrix*/
    /*these are the important variables*/
    /*num_B0 = default we leave it for now */
    fprintf(stderr, "Read diffusion directions from file \n"); 
    if(((int) fileId_cv) == -1){
	set_tensor_orientationsAGP();
	fprintf(stderr, "Reading from tensor.dat orientations \n");
    }
    else{     
	fprintf(stderr, "Overwrite tensor.dat orientations \n");
	/*distinguish between modes */
	switch(dpfgSBB.mode){
	    case SSE:
		readAndSetDiffGradTable_AGP(fileId_cv,"DiffusionVectors_dPFG_SSE",1);
		break;
	    case DSE:
		readAndSetDiffGradTable_AGP(fileId_cv,"DiffusionVectors_dPFG_DSE",1);
		break;				
	    case HSE:
		readAndSetDiffGradTable_AGP(fileId_cv,"DiffusionVectors_dPFG_HSE",1);
		break;
	    case DTI:
		readAndSetDiffGradTable_AGP(fileId_cv,"DiffusionVectors_sPFG_SSE",0);
		break;			  
	    default:
		readAndSetDiffGradTable_AGP(0,"DiffusionVectors_sPFG_SSE",0);
		break;
	}
      
    }   




    /********************************************************************/
    /* SETUP TRANSMIT & RECEIVE FREQUENCIES                             */
    /********************************************************************/
    rfphastab = (int *)AllocNode(nbang * sizeof(int));
    rf1_freq = (int *)AllocNode(opslquant * sizeof(int));
    rf2_freq = (int *)AllocNode(opslquant * sizeof(int));
    slcReorderIndex = (int *)AllocNode(opslquant * sizeof(int));
    slcReorderIndexMB = (int *)AllocNode(opslquant/mb_factor * sizeof(int));
    theta_freq = (int *)AllocNode(opslquant * sizeof(int));
    freq = (int *)AllocNode(opslquant * sizeof(int));
    tf = (int *)AllocNode((w.ni+1)*sizeof(int));

    /*setup all transmit and receive frequencies based on sequential ordering*/
    int slidx;
    for( slidx = 0; slidx < opslquant; slidx++ )
        rsp_info[slidx].rsprloc = 0;

    setupslices(freq, rsp_info, opslquant, 0.0, oprbw, opfov, TYPREC);

    /*slice by slice increment*/
    for(slidx=0;slidx<opslquant;slidx++){
        rf1_freq[slidx] = (slidx*(opslthick+opslspace))*GAM * excSBB.a_gzrf / (10 * TARDIS_FREQ_RES);

        if( oppseq == PSD_SE )
        rf2_freq[slidx] = (slidx*(opslthick+opslspace))*GAM * dpfgSBB.m_sbb180.a_gzrf / (10 * TARDIS_FREQ_RES);
    }

    /*BZ shuffle slice ordering index*/
    sliceReordering(slcReorderIndex,opslquant,opslice_order);
    sliceReordering(slcReorderIndexMB,opslquant/mb_factor,opslice_order);
    

    /*Initialize all SBB's for first scan */
    sbb_exc_psdinit(&excSBB);
    if( oppseq == PSD_SE ){
        sbb_refoc_psdinit(&SBB180);
        /*sbb_refoc_crusherTheme(&SBB180,1); *//*crusher theme 1 for dual refocusing only*/
    }
	sbb_dw_dpfg_psdinit(&dpfgSBB);
	sbb_dw_dpfg_crusherTheme(&dpfgSBB,crushertheme);

    if(opfat==1){
        sbb_nonSelective_psdinit(&FatSatSBB);
        setfrequency((int)(satoff/TARDIS_FREQ_RES), &FatSatSBB.rf, 0);
    }

    /*sliding window interleaves */
    int delt;
    int esp;
    esp = (w.epi_flat_res + 2*w.epi_ramp_res)*4us;
    delt = RDN_GRD((int)((float)esp/(float)(w.ni)));
    for(iv=0;iv<w.ni;iv++){  
      tf[iv] = (iv)*delt + GRAD_UPDATE_TIME;
    }

    bangn = 0; /* init spoiler counter */
    diff_counter = 0; /*init diffusion counter*/
    dabop = DABSTORE;
    iv_ro = 0;

    /* initial startseq to avoid an error related to the loading of the waveforms
     otherwise, I get the pulse generation was still active when EOS SSP was received message
     */

    boffset (off_seq_pause);
    startseq(0, MAY_PAUSE);

    /********************************************************************/
    /* PRESCAN loop                                                     */
    /********************************************************************/
    if( pre > 0)
    {

        fprintf(stderr,"entering prescan loop\n");

        for( i = 0; i < nextra; i++ )
        { /*  disdaqs  */
            trig = (gating == TRIG_AUX) ? TRIG_INTERN : gating;
            for( sliceindex = 0; sliceindex < opslquant/mb_factor; sliceindex++ )
            {
                ifr = 0;
                /* use central interleaf */
                iv = w.i_ctr;
                dtype = DABOFF;
                doleaf();
            }

        }
        for( i = 0; i < 10000; i++ )
        { /* get data  */
            trig = (gating == TRIG_AUX) ? TRIG_INTERN : gating;
            for( sliceindex = 0; sliceindex < opslquant/mb_factor; sliceindex++ )
            {
                dtype = DABON;
                ifr = 0;
                /* use central interleaf */
                iv = w.i_ctr;
                doleaf();
            }

        }

        rspexit();
    }

    /********************************************************************/
    /* Transition into steady state (dummy loop) after calibration scans*/
    /********************************************************************/
    trig = gating;
    /*initialize frame counters */
    ifr = 0;
    ifr_start = 0;
    

      /********************************************************************/
      /* single band calibration loop                                     */
      /********************************************************************/
      if(mb_single_calib_cv){

	  /*set z-gradient (=caipi blips) to zero */
	  setiamp(0, &((&(w_psd[0]))->gzw) , 0);

	  /*set wave to single band pulse in case of multiband pulse*/
	  singleSlice_flag  = 0;
	  if(mb_single_calib_cv && (mb_factor>1)){
	      singleSlice_flag  = 1;
	      singleSlice_counter = 0;
	      sbb_exc_setwave(&excSBB,mb_factor); /*[0]...[mb_factor-1] == pomp-pulses; [mb_factor] == single-band-pulse */
	      if( oppseq == PSD_SE )
                 sbb_dw_dpfg_setwave(&dpfgSBB,mb_factor);
	  }

	  /*turn off diffusion gradients*/
	  turnoff_diff = 1;

	  /********************************************************************/
	  /* fully sampled central region in case of in-plane acceleration    */
	  /********************************************************************/
	  fprintf(stderr,"central region single band loop \n");
	  singleSlice_counter = 0;
	  singleSlice_flag = 1;

	  /*decrease blip moment*/
	  fprintf(stderr,"w_psd->ia_gyw/w.ni = %d  \n",w_psd->ia_gyw/ni_cv);
	  for(i=0;i<opnecho_imag;i++){
	    setiamp(w_psd->ia_gyw/ni_cv, &((&(w_psd[i]))->gyw) , 0);
	  }
	  /*setiamp(w_psd->ia_gyw/w.ni, &w_psd->gyw , 0);*/
	    
	  iv = 0 ; /*we only need the first interleave*/
	    for( ifr = ifr_start; ifr < ifr_start + mb_factor; ifr++ ){
		for( sliceindex = 0; sliceindex < (opslquant/mb_factor); sliceindex++ )
                {
                    dtype = DABON;
                    doleaf();
                }
		singleSlice_counter++;
	      }
	  ifr_start = ifr;
	  

	  
	  /*Fully samples central region blip down instead of wide fov*/
	  if(mb_single_calib_blip_down_cv){
	  fprintf(stderr,"central region single band loop (blip down) \n");
	  singleSlice_counter = 0;
	  
	  /*decrease and invert blip moment*/
	  for(i=0;i<opnecho_imag;i++){
	    setiamp(-w_psd->ia_gyw/ni_cv, &((&(w_psd[i]))->gyw) , 0);
	  }
	  /*setiamp(-w_psd->ia_gyw/w.ni, &w_psd->gyw , 0);*/
	    
	  iv = 0 ; /*we only need the first interleave*/
	    for( ifr = ifr_start; ifr < ifr_start + mb_factor; ifr++ ){
		for( sliceindex = 0; sliceindex < (opslquant/mb_factor); sliceindex++ )
                {
                    dtype = DABON;
                    doleaf();
                }
		singleSlice_counter++;
	      }
	  ifr_start = ifr;
	  }

	  
	  /*frame counter ifr was increased and restore y-gradient amplitude*/
	  setiamp(w_psd->ia_gyw, &w_psd->gyw , 0);
	  
	  }
	  /*End of central region single band calibration scan*/
	  

	
      /********************************************************************/
      /*Set everything back to normal after calibration scans are done*/
      /********************************************************************/ 
	fprintf(stderr,"reset calibration changes \n");
	for(i=0;i<opnecho_imag;i++){
	  setiamp(w_psd->ia_gzw, &((&(w_psd[i]))->gzw) , 0); /*enable caipi blips again */
	  setiamp(w_psd->ia_gyw, &((&(w_psd[i]))->gyw) , 0); /*y-gradient */
	}
	  
        singleSlice_flag  = 0; /*disable single slice mode */
        turnoff_diff = 0;	/*turn diffusion gradients back on*/
        ifr_start = ifr;	/*set start frame number to most recent counter*/

        /*reset wave to K0_index (=no phase modulation across slices*/
        sbb_exc_setwave(&excSBB,K0_Idx);
	sbb_dw_dpfg_setwave(&dpfgSBB,K0_Idx);



    /********************************************************************/
    /* Transition into steady state (dummy loop after calibration scans)*/
    /********************************************************************/
    for( i = 0; i < nextra; i++ )
    {
	fprintf(stderr,"transition into steady state.\n");
        for( sliceindex = 0; sliceindex < opslquant/mb_factor; sliceindex++ )
        {
            dtype = DABOFF;
            doleaf();
        }
        if( gating == TRIG_ECG )
            trig = TRIG_ECG;

    }


    /********************************************************************/
    /* SCAN loop                                                        */
    /********************************************************************/
    fprintf(stderr,"scan loop \n"); fflush(stderr);
    mb_counter = 0;
    diff_counter = 0;
    scan_counter = 0;
    iv_ro = 0;
      
    
    for( ifr = ifr_start; ifr < nframes; ifr++ )
    {
        /*if we have more than one slice...we can do rf-encoding */
        if(mb_rfmod_cv && (mb_factor > 1)){

            sbb_exc_setwave(&excSBB,mb_counter);
            if( oppseq == PSD_SE )
                sbb_dw_dpfg_setwave(&dpfgSBB,mb_counter);
            mb_counter++;
            if(mb_counter > (mb_factor-1))
                mb_counter = 0;

        }
        else{
            /*BZ this should do nothing and leave the wave where it is*/
            sbb_exc_setwave(&excSBB,-1);
            if( oppseq == PSD_SE )
                sbb_dw_dpfg_setwave(&dpfgSBB,-1);
        }     
  
	
        for( iv = 0; iv < data_interleaves; iv = iv + 1 )
        {
	  
	  /*which of the interleaves(=waveforms) do we want to acquire*/
	  iv_ro = iv;
	  if(epi_blip_up_down_cv){
	    
	    if(ni_cv ==1)
	      iv_ro = iv;
	    else
	      iv_ro = iv + (ni_cv)*iv;
	  
	    /*fprintf(stderr,"iv_ro %i \n",iv_ro); fflush(stderr);*/
	  }
	  
	    
            for( sliceindex = 0; sliceindex < opslquant/mb_factor; sliceindex++ )
            {

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rsp_scan_update_before
%endif

                dtype = DABON;
                doleaf();

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rsp_scan_update_after
%endif

            }/*end of slice loop */
        }/*end of interleave loop */
    
      /*by defintion we should increase diff_counter here */
      diff_counter++;
      
      /*blip up down for 2nd b0 scan only if not up down for whole timeseries*/
      if((num_B0 > 1) && (epi_blip_up_down_cv ==0)){
	
	  if(scan_counter==0){
	      fprintf(stderr,"blip down b0 scan %i \n",0);
	      setiamp(-w_psd->ia_gyw, &w_psd->gyw , 0);
	  }
	  if(scan_counter==1){
	    fprintf(stderr,"reset blip down %i \n",0);
	    setiamp(w_psd->ia_gyw, &w_psd->gyw , 0);
	  }
      }    
      
    scan_counter++;   
    }/*end of time frame loop */

%ifdef RTMOCO
@inline rtMoCo.e rtMoCo_rsp_scan_finish
%endif

/* tell 'em it's over */
boffset (off_pass);
setwamp(SSPD + DABPASS + DABSCAN, &endpass, 2);
settrigger(TRIG_INTERN, 0);
iv = 0;
sliceindex = 0;
ifr = 0;

startseq(0, MAY_PAUSE); /* fat lady sings */

return SUCCESS;
}

/*
 * function doleaf()
 *
 * this function does most of the things connected with a single
 * spiral excitation.  in keeping with standard epic practices,
 * it secretly uses a variety of global variables, including
 * opslquant, savrot, nl, nframes, fxmit, frec, echo1, omrf1, core
 * and gating.
 *
 * arguments
 *     leafn  -- interleaf number.       0 <= leafn  < nl.
 *     framen -- temporal frame number.  0 <= framen < nframes.
 *     slicen -- slice number.           0 <= slicen < opslquant.
 *     bangn  -- rf bang number.         0 <= bangn  < nbang.
 *     dabop  -- dabop for loaddab.
 *     dtype  -- type of data acquisition (DABON or DABOFF).
 *
 */

void doleaf()
{

    int viewn;
    int i;
    int sliceindex2; /* this is the index of the slice in which we take into account
     * whether slices are sequential or interleaved
     */

    /*BZ hack for now */
    sliceindex2 = sliceindex;


    if( debugstate ){
        fprintf(stderr, "doleaf\n");
        fprintf(stderr, "\tifr : %d iv : %d sliceindex : %d\n", ifr, iv, sliceindex);
        fflush(stderr);
    }


    /*-------------------------------------------*/
    /* Diffusion encoding                        */
    /*-------------------------------------------*/
    if( opdiffuse == 1 )
    {
        if( turnoff_diff == 0 )
        {	

	  sbb_dw_dpfg_diffstep(&dpfgSBB,TENSOR_AGP,diff_counter);
	  /*diffstep(diff_counter);*/
	  /*fprintf(stderr, "\diff_counter : %d \n", diff_counter);*/
	  /*fprintf(stderr, "D= #%d, X = %f, Y=%f, Z= %f\n", diff_counter, TENSOR_AGP[0][diff_counter], TENSOR_AGP[1][diff_counter], TENSOR_AGP[2][diff_counter] );*/
        
	}
        else
        {
            /*diffstep(NO_DIF_GRADS);*/
            sbb_dw_dpfg_diffstep(&dpfgSBB,TENSOR_AGP,NO_DIF_GRAD);
        }
    }
    /* ------------------------------------------------------ */


    /* for passing multiple pfiles */
    if( pass_multiple_pfiles == 0 )
    {
        pfile_pass_state = KEEP_CURRENT; /* one pfile */
    }
    else
    { /* one pfile per diffusion direction */
        /* is this the last shot of a diffusion direction ? */
        if( iv == (w.ni / opnecho_imag - 1) && (sliceindex == opslquant - 1) )
        {
            /* is this the last diff dir ? */
            if( ifr == nframes - 1 )
            {
                pfile_pass_state = LAST_PFILE_DONE;
            }
            else
            {
                pfile_pass_state = NEXT_PFILE;
            }
        }
        else
        {
            pfile_pass_state = KEEP_CURRENT;
        }

    }


    /*-------------------------------------------*/
    /* Set transmit and receive frequencies      */
    /*-------------------------------------------*/
    for( i = 0; i < opnecho_imag; i++ )
    {
        /*setfrequency(freq[sliceindex], &(w_psd[i].echo), 0);*/ /*No freq shifts for now*/
    }
    if( w_nav.ro_type != -1 )
    {
        setfrequency(freq[sliceindex], &(w_psd_nav.echo), 0);
    }
    if( epi_ref_cv )
    {
        setfrequency(freq[sliceindex], &(w_psd_epi_ref.echo), 0);
    }

    /*BZ transmit frequencies, sliceindex = temporal order*/
    if(mb_single_calib_cv && (singleSlice_flag  == 1)){
      
      sbb_exc_setfrequency(&excSBB,rf1_freq[slcReorderIndex[sliceindex + singleSlice_counter*(opslquant/mb_factor)]]);
      if(oppseq == PSD_SE)
	  sbb_dw_dpfg_setfrequency(&dpfgSBB,rf2_freq[slcReorderIndex[sliceindex + singleSlice_counter*(opslquant/mb_factor)]]);
    }
    else{
      sbb_exc_setfrequency(&excSBB,rf1_freq[slcReorderIndexMB[sliceindex]]);
      if(oppseq == PSD_SE){
        sbb_dw_dpfg_setfrequency(&dpfgSBB,rf2_freq[slcReorderIndexMB[sliceindex]]);
      }
    }
  
    /* Gradient Echo */
    if( oppseq == 2 )
    {
        /*setiphase(rfphastab[bangn], &(w_psd[i].echo), 0);*/
    }
    if( w_nav.ro_type != -1 )
    {
        setiphase(rfphastab[bangn], &(w_psd_nav.echo), 0);
    }
    if( epi_ref_cv )
    {
        setiphase(rfphastab[bangn], &(w_psd_epi_ref.echo), 0);
    }
    /*(bangn)++;*/
    bangn = bangn % nbang; /*  for mps2  */




    /* Spin Echo */
    if( oppseq == 1 )
    {
        /* Dual */
        if( opdualspinecho == 1 )
        {
            for( i = 0; i < opnecho_imag; i++ )
                setiphase(0, &(w_psd[i].echo), INSTRALL);
            if( w_nav.ro_type != -1 )
                setiphase(0, &(w_psd_nav.echo), INSTRALL);
            if( epi_ref_cv )
            {
                setiphase(0, &(w_psd_epi_ref.echo), INSTRALL);
            }
        }
        else
        {        /* Single */
            for( i = 0; i < opnecho_imag; i++ )
                setiphase(0, &(w_psd[i].echo), INSTRALL);
            if( w_nav.ro_type != -1 )
                setiphase(0, &(w_psd_nav.echo), 0);
            if( epi_ref_cv )
            {
                setiphase(0, &(w_psd_epi_ref.echo), 0);
            }
        }
    }

    if( debugstate )
    {
        fprintf(stderr, "\tset frequency and phase\n");
        fflush(stderr);
    }

/* load the next readout */
/* if I dont put this if, I get end of EOS received etc error */
if( pre == 0 )
{
    if( iv >= 0 )
    {
        for( i = 0; i < opnecho_imag; i++ )
        {
            /* sequential ordering of the interleaves */
	    load_ro(&w, &(w_psd[i]), (iv_ro * opnecho_imag + i) % w.ni);
        }
    }
    if( w_nav.ro_type != -1 )
    {
        if( iv >= 0 )
        {
            /* navigator has a single echo*/
            load_ro(&w_nav, &w_psd_nav, iv % w_nav.ni);
        }
    }
    if( epi_ref_cv )
    {
        if( iv >= 0 )
        {
            /* navigator has a single echo*/
            load_ro(&w_epi_ref, &w_psd_epi_ref, 0);
        }
    }

}
else if( pre > 0  )
{
    /* load the center interleaf for maximum signal
     * this part crashes the scan because it takes too much time to load
     * thus, I am only loading the first echo
     * however, this won't work for a sequence like sage because the maximum
     * signal does not occur on the first echo for sage.
     * try to address that later
     */
    /* UPDATE- trying to fix this by adding an additional startseq at the beginning */
    for( i = 0; i < opnecho_imag; i++ )
    {
        load_ro(&w, &(w_psd[i]), w.i_ctr);
    }
    if( w_nav.ro_type != -1 )
    {
        load_ro(&w_nav, &w_psd_nav, w.i_ctr % w_nav.ni);
    }
    if( epi_ref_cv )
    {
        load_ro(&w_epi_ref, &w_psd_epi_ref, 0);
    }

}

/* set up dab */

/* rtMoCo */
/* if prescan, load the data to the first view */
if( pre > 0 )
{
    for( i = 0; i < opnecho_imag; i++ )
    {
        /*loaddab(&(w_psd[i].echo), sliceindex2, 0, dabop, 0, dtype, dabmask);*/ /*this is Murat */
        loaddab(&(w_psd[i].echo), sliceindex, 0, dabop, 0, dtype, dabmask); /*BZ*/
    }
    if( w_nav.ro_type != -1 )
    {
        /*loaddab(&(w_psd_nav.echo), sliceindex2, 1, dabop, 0, dtype, dabmask); *//*murat*/
        loaddab(&(w_psd_nav.echo), sliceindex, 1, dabop, 0, dtype, dabmask);
    }
    if( epi_ref_cv )
    {
        /*loaddab(&(w_psd_epi_ref.echo), sliceindex2, 2, dabop, 0, dtype, dabmask);*/ /*murat */
        loaddab(&(w_psd_epi_ref.echo), sliceindex, 2, dabop, 0, dtype, dabmask);
    }
}

/*else load the data*/
else
{
    if( pass_multiple_pfiles == 0 )
    {

        viewn = ifr * data_interleaves + iv * opnecho_imag + 1;
    }
    else
    {
        viewn = iv * opnecho_imag + 1;
    }

    if( debugstate )
    {
        fprintf(stderr, "\tloaddab %d\n", viewn);
        fflush(stderr);
    }

    for( i = 0; i < opnecho_imag; i++ )
    {
	if(mb_single_calib_cv)
        loaddab(&(w_psd[i].echo), sliceindex%(opslquant/mb_factor), 0, dabop, viewn + i, dtype, dabmask);
	else
	loaddab(&(w_psd[i].echo), sliceindex, 0, dabop, viewn + i, dtype, dabmask);
    }
    if( w_nav.ro_type != -1 )
    {
        loaddab(&(w_psd_nav.echo), sliceindex, 1, dabop, viewn, dtype, dabmask);
    }
    if( epi_ref_cv )
    {
        loaddab(&(w_psd_epi_ref.echo), sliceindex, 2, dabop, viewn, dtype, dabmask);
    }
}


if( debugstate )
{
    fprintf(stderr, "\tstartseq %d\n", viewn);
    fflush(stderr);
}

/* reset offset and go for it */
boffset (off_seqcore);
startseq(sliceindex, MAY_PAUSE);

trig = TRIG_INTERN;

} /* end of function doleaf() */


void sliceReordering(int* reorderIndex,int Ntot,int mode){
    /*reorderIndex = returns array with shuffled indices */
    /*N = total number of excited slices*/
    /*mode = {0=linear, 1=interleaved,...}*/

    int l;

    /*even number of slices */
    if(Ntot%2 == 0){
        for(l=0;l<Ntot/2;l++){
            reorderIndex[l] = 2*l;
	    reorderIndex[l + Ntot/2] = 2*l+1;

        }
    }

    /*linear re-ordering*/
    if(mode ==0){
        for(l=0;l<Ntot;l++){
            reorderIndex[l] = l;
        }
    }
}


@pg
/********************************************
 * dummylinks
 *
 * This routine just pulls in routines from
 * the archive files by making a dummy call.
 ********************************************/
void dummylinks()
{
epic_loadcvs("thefile"); /* for downloading CVs */
}


